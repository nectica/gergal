<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PedidoItem extends Model
{
    use HasFactory;
    protected $table = 'pedidos_items';

    public function pedido (){
        return $this->belongsTo(Pedido::class, 'pedidos_id');
    }

    public function producto(){
        return  $this->belongsTo(Producto::class, 'producto_id');
	}
	public function getProductoPrecioUnitarioAttribute($value){
		return '$'.$value;
	}
}
