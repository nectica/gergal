<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Pedido extends Model
{
    use HasFactory, Sortable;
    protected $table = 'pedidos';
    protected $sortable = ['cli_id', 'rev_id', 'precio_total', 'estado', 'fecha_creado', 'fecha_modif'];
    protected $fillable = ['estado'];
    const CREATED_AT = 'fecha_creado';
    const UPDATED_AT = 'fecha_modif';
    public function usuario(){
        return $this->belongsTo(Usuario::class, 'cli_id');
    }
    public function revendedor(){
        return $this->belongsTo(Usuario::class, 'rev_id');
    }
    public function productoItem(){
        return $this->hasMany(PedidoItem::class, 'pedidos_id');
    }
    public function productos(){
        return $this->belongsToMany(Producto::class, 'pedidos_items', 'pedidos_id', 'producto_id')->withPivot(['producto_precio_unitario', 'cant']);
    }
    public function estadoR(){
        return $this->belongsTo(Estado::class, 'estado');
	}
	public function getPrecioTotalAttribute($value){
		return '$'.$value;
	}
	public function getProductoPrecioUnitarioAttribute(){
		return '$'.$this->pivot->producto_precio_unitario;
	}
	public function setPrecioTotalAttribute($value){
		if (str_contains($value, '$')  ) {
			$this->attributes['precio_total'] = substr($value,  0, count($value) );
		}else {
			$this->attributes['precio_total'] = $value ;
		}
	}
}


