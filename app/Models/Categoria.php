<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Categoria extends Model
{
    use HasFactory, Sortable;
    protected $table = 'categorias';
    protected $sortable = ['nombre'] ;
    protected $fillable = ['nombre', 'imagen'];
    public $timestamps = false;

    public function productos(){
        return $this->hasMany(Producto::class, 'categoria');
    }
}
