<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Usuario extends Model
{
    use HasFactory, Sortable;
    const CREATED_AT = 'fecha_creado';
    const UPDATED_AT = 'fecha_modif';
    protected $fillable = ['tipo_usuario_id', 'nombre', 'apellido', 'email', 'dni',
	 'password', 'telefono',
	 'direccion', 'latitud',
	 'longitud', 'estado',
	 'calle_nro', 'nombre_calle',
	 'localidad',  'partido',
	 'provincia', 'pais',
	 'codigo_postal'
	 , 'direccion2', 'barrio'
	];
    protected $sortable = ['tipo_usuario_id', 'nombre', 'apellido', 'email', 'dni',
	 'password', 'telefono', 'direccion', 'latitud', 'longitud', 'estado',
	 'calle_nro', 'nombre_calle',
	 'localidad',  'partido',
	 'provincia', 'pais','direccion2', 'barrio',
	 'codigo_postal'];
	 protected $appends = ['full_address'];

    public function tipoUsuario(){
        return $this->belongsTo(TipoUsuario::class, 'tipo_usuario_id');
    }
    public function Pedidos(){
        return $this->hasMany(Pedido::class, 'cli_id');
    }

	public function getFullAddressAttribute(){
		$fullAddress = '';
		if ($this->longitud != 0 && $this->latitud != 0) {
			if ($this->localidad ) {
				$fullAddress = $this->nombre_calle.' '.$this->calle_nro.', '.$this->partido.', '.$this->provincia .', '.$this->pais ;
			}else {
				$fullAddress = $this->nombre_calle.' '.$this->calle_nro.', '.$this->localidad.', '.$this->partido.', '.$this->provincia .', '.$this->pais ;
			}
		}
		return $fullAddress;

	}
}
