<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Estado extends Model
{
    use HasFactory, Sortable;
    protected $table = 'estados';

    protected $fillable = ['descripcion'];

    public function pedidos(){
        return $this->hasMany(Pedido::class, 'estado');
    }
}
