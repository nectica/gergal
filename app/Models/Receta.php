<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Receta extends Model
{
    use HasFactory, Sortable;
    const CREATED_AT = 'fecha_creado';
    const UPDATED_AT = 'fecha_modif';
	public $sortable = ['titulo', 'ingredientes', 'preparacion',  'image_path', ];
	protected $fillable = ['titulo', 'ingredientes', 'preparacion',  'image_path', ];
}
