<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class MinimoCompra extends Model
{
    use HasFactory, Sortable;

	protected $table = 'minimo_compra';
	protected $fillable = ['monto_minimo_compra', 'minimo_compra_revendedor'];
	public $timestamps = false;
}
