<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Contact extends Model
{
    use HasFactory, Sortable;
    protected $table = 'contactos_form';
    protected $sortable = ['email', 'remitente', 'asunto', 'mensaje'];
    const CREATED_AT = 'fecha_creado';
    const UPDATE_AT  = 'fecha_modif';
}
