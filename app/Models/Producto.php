<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Kyslik\ColumnSortable\Sortable;

class Producto extends Model
{
	use HasFactory, Sortable;
	protected $table = 'productos';
	public $sortable = ['nombre', 'descripcion', 'categoria',  'sub_categoria', 'propiedades',
		'info_nutricional', 'precio_lista', 'precio_revendedor',
		'estado', 'cantidad_en_stock', 'destacado', 'novedades', 'orden', 'estado_revendedores'
	];
	protected $fillable = ['nombre', 'descripcion', 'categoria',  'sub_categoria', 'propiedades',
		'info_nutricional', 'precio_lista', 'precio_revendedor', 'estado', 'image_path',
		'hover_image_path', 'cantidad_en_stock', 'destacado', 'novedades', 'orden', 'estado_revendedores'
	];
	protected $appends = ['producto_precio_unitario'];

	const CREATED_AT = 'fecha_creado';
	const UPDATED_AT = 'fecha_modif';
	public function categoriaR(){
		return $this->belongsTo(Categoria::class, 'categoria');
    }
    public function subcategoria(){
		return $this->belongsTo(Categoria::class, 'sub_categoria');
    }
    public function pedidos(){
        return $this->belongsToMany(Pedido::class, 'pedidos_items', 'producto_id', 'pedidos_id')->withPivot(['producto_precio_unitario', 'cant']);
	}
	public function getPrecioListaAttribute($value){
		return '$'.$value;
	}
	public function getPrecioRevendedorAttribute($value){
		return '$'.$value;
	}
	public function getProductoPrecioUnitarioAttribute(){
		return '$'.$this->pivot->producto_precio_unitario;
	}
	public function setPrecioListaAttribute($value){
		if (str_contains($value, '$')  ) {
			$this->attributes['precio_lista'] = substr($value, 1 );
		}else {
			$this->attributes['precio_lista'] = $value ;
		}
	}
	public function setPrecioRevendedorAttribute($value){
		if (str_contains($value, '$')  ) {
			$this->attributes['precio_revendedor'] = substr($value, 1 );
		}else {
			$this->attributes['precio_revendedor'] = $value ;
		}
	}

}

