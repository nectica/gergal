<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nombre' => 'required|min:4',
            /* 'descripcion' => 'required|min:4',
            'propiedades' => 'required|min:4',
            'info_nutricional' => 'required|min:4', */
            'img' => 'image|mimes:png,jpg|max:512',
            'img_hoover' => 'image|mimes:png,jpg|max:512',
            /* 'categoria' => 'required',
            'sub_categoria' => 'required', */
            'precio_lista' => 'required|numeric',
            'precio_revendedor' => 'required|numeric',
			'orden' => 'nullable|numeric',
        ];
    }
}
