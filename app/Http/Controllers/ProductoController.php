<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductoRequest;
use App\Http\Requests\UpdateProductoRequest;
use App\Models\Categoria;
use App\Models\Producto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class ProductoController extends Controller
{
	function __construct()
	{
		$this->middleware('permission:product-list|product-create|product-edit|product-delete', ['only' => ['index', 'store', 'show', 'filter']]);
		$this->middleware('permission:product-create', ['only' => ['create', 'store']]);
		$this->middleware('permission:product-edit', ['only' => ['edit', 'update']]);
		$this->middleware('permission:product-delete', ['only' => ['destroy']]);
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		Session::forget('filter');
		Session::forget('redirect');

		$productos = Producto::sortable()->orderBy('nombre', 'asc')->paginate(5);
		$categorias =  Categoria::all();
		return view('product.index', compact('productos', 'categorias'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$categorias =  Categoria::all();
		return view('product.create', compact('categorias'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(StoreProductoRequest $request)
	{
		$imagePath = 'public/storage/';
		$imagePathHover = 'public/storage/';
		$merged = [];
		$merged['estado'] =  isset($request->estado);
		$merged['destacado'] =  isset($request->destacado);
		$merged['novedades'] =  isset($request->novedades);
		$merged['estado_revendedores'] =  isset($request->estado_revendedores);
		if ($request->hasFile('img')) {
			$merged = array_merge($merged, $this->saveImage($request, 'img'));
		}
		if ($request->hasFile('img_hoover')) {
			$merged = array_merge($merged, $this->saveImage($request, 'img_hoover', 'hover_image_path'));
		}
		if( $request->orden === null){
			$merged['orden'] = 1000;
		}
		Producto::create(array_merge($request->all(), $merged));

		return redirect()->route('productos.index')->with('success', 'se ha creado correctamente el producto.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show(Producto $producto)
	{
		return view('product.show', compact('producto'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Producto $producto)
	{
		Session::put('redirect', Session::get('_previous')['url']);
		$categorias =  Categoria::all();
		return view('product.update', compact('producto', 'categorias'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(UpdateProductoRequest $request, Producto $producto)
	{
		$merged = [];
		if ($request->hasFile('img')) {
			$imagePath = 'public/storage/';
			$imgMain = pathinfo($producto->image_path, PATHINFO_BASENAME);
			Storage::delete('producto/' . $imgMain);
			$extesion = $request->file('img')->extension();
			$fileOriginalName = pathinfo($request->file('img')->getClientOriginalName(), PATHINFO_FILENAME);
			$date = date('Y_m_d_H_i_s');
			$nameFile = $date . '_' . $fileOriginalName . '.' . $extesion;
			$path = $request->file('img')->storeAs('producto', $nameFile);
			$imagePath .= $path;
			$merged['image_path'] = $imagePath;
		}
		if ($request->hasFile('img_hoover')) {
			$imagePathHover = 'public/storage/';
			$imgHover = pathinfo($producto->hover_image_path, PATHINFO_BASENAME);
			Storage::delete('producto/' . $imgHover);
			$extesion = $request->file('img_hoover')->extension();
			$fileHoverOriginalName = pathinfo($request->file('img_hoover')->getClientOriginalName(), PATHINFO_FILENAME);
			$date = date('Y_m_d_H_i_s');
			$nameFileHoover = $date . '_' . $fileHoverOriginalName . '_hover' . '.' . $extesion;
			$pathHover = $request->file('img_hoover')->storeAs('producto', $nameFileHoover);
			$imagePathHover .= $pathHover;
			$merged['hover_image_path'] = $imagePathHover;
		}
		$merged['estado'] =  isset($request->estado);
		$merged['destacado'] =  isset($request->destacado);
		$merged['novedades'] =  isset($request->novedades);
		$merged['estado_revendedores'] =  isset($request->estado_revendedores);
		if( $request->orden === null){
			$merged['orden'] = 1000;
		}
		/* dd( array_merge($request->all(), $merged)); */
		$producto->update(array_merge($request->all(), $merged));
		return redirect()->away(Session::get('redirect'))/* ->route('productos.index') */->with('success', 'se ha actualizado correctamente el producto.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Producto $producto)
	{
		$imgHover = pathinfo($producto->hover_image_path, PATHINFO_BASENAME);
		Storage::delete('producto/' . $imgHover);
		$imgMain = pathinfo($producto->image_path, PATHINFO_BASENAME);
		Storage::delete('producto/' . $imgMain);
		$producto->delete();
		return redirect()->route('productos.index')->with('success', 'se ha borrado correctamente el producto.');
	}
	public function filter(Request $request)
	{
		Session::put('_previous', ['url' => route('productos.filter.get')]);

		$productos = [];
		if (Session::has('filter') && $request->nombre == null) {
			$productos = Session::get('filter');
		} else if ($request->nombre) {
			$productos = Producto::sortable()
				->where('nombre', 'like', '%' . $request->nombre . '%')
				->orWhere('descripcion', 'like', '%' . $request->nombre . '%')
				->orWhere('propiedades', 'like', '%' . $request->nombre . '%')
				->orWhere('info_nutricional', 'like', '%' . $request->nombre . '%')
				->orderBy('fecha_modif', 'DESC')
				->get();
			Session::put('filter', $productos);
		} else {
			return redirect()->route('productos.index');
		}
		$categorias =  Categoria::all();

		return view('product.index', compact('productos', 'categorias'));
	}
	public function saveImage($request, $nameFileParameter, $image_parameter = 'image_path')
	{
		$imagePath = 'public/storage/';
		$extension = $request->file($nameFileParameter)->extension();
		$fileOriginalName = pathinfo($request->file($nameFileParameter)->getClientOriginalName(), PATHINFO_FILENAME);
		$date = date('Y_m_d_H_i_s');
		$nameFile = $date . '_' . $fileOriginalName . '.' . $extension;
		if ($image_parameter == 'hover_image_path') {
			$nameFile = $date . '_' . $fileOriginalName . '_hover.' . $extension;
		}
		$path = $request->file($nameFileParameter)->storeAs('producto', $nameFile);
		$merged[$image_parameter] = $imagePath . $path;
		return $merged;
	}
}
/* $extension = $request->file('img')->extension();
			$extensionHover = $request->file('img_hoover')->extension();
			$fileOriginalName = pathinfo($request->file('img')->getClientOriginalName(), PATHINFO_FILENAME);
			$fileHoverOriginalName = pathinfo($request->file('img_hoover')->getClientOriginalName(), PATHINFO_FILENAME);
			$date = date('Y_m_d_H_i_s');
			$nameFile = $date . '_' . $fileOriginalName . '.' . $extension;
			$nameFileHover = $date . '_' . $fileHoverOriginalName . '_hover.' . $extensionHover;
			$path = $request->file('img')->storeAs('producto', $nameFile);
			$pathHover = $request->file('img_hoover')->storeAs('producto', $nameFileHover);
			$imagePath .= $path;
			$imagePathHover .= $pathHover;
			$merged['image_path'] = $imagePath;
			$merged['hover_image_path'] = $imagePathHover; */
