<?php

namespace App\Http\Controllers;

use App\Models\Estado;
use Illuminate\Http\Request;

class EstadoController extends Controller
{
    function __construct(){
        $this->middleware('permission:status-list|user-create|status-edit|status-delete', ['only' => ['index', 'store', 'filter']]);
        $this->middleware('permission:status-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:status-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:status-delete', ['only' => ['destroy']]);
    }/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $estados = Estado::sortable()
            ->orderBy('id', 'DESC')
            ->paginate(5);
        return view('status.index', compact('estados'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('status.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'descripcion' => 'required|min:4',
        ];
        $request->validate($rules);

        Estado::create($request->all());
        return redirect()
            ->route('estados.index')
            ->with('success', 'Se creo correctamente el estado');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Estado $estado)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Estado $estado)
    {
        return  view('status.update', compact('estado'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Estado $estado)
    {
        $rules = [
            'descripcion' => 'required|min:4',
        ];
        $request->validate($rules);
        $estado->update($request->all());
        return redirect()
            ->route('estados.index')
            ->with('success', 'Se actualizó correctamente el estado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Estado $estado)
    {
        $estado->delete();
        return redirect()
            ->route('estados.index')
            ->with('success', 'Se elimino correctamente el estado');
    }
    public function filter(Request $request){
        $estados = Estado::where('descripcion', 'like', '%'.$request->nombre.'%')
            ->get();

        return view('status.index', compact('estados') );
    }
}
