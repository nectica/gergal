<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use App\Models\Pedido;
use App\Models\Producto;
use App\Models\Receta;
use App\Models\User;
use DateInterval;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Show the application dashboard.
	 *
	 * @return \Illuminate\Contracts\Support\Renderable
	 */
	public function index()
	{
		$interval = new DateInterval('P1W');
		$now = new  DateTime('now');

		$lastWeek = new  DateTime('now');
		$nextWeek = new  DateTime('now');
		$now->setTime(0, 0);
		$lastWeek->setTime(0, 0);
		$lastWeek->sub($interval);


		$pedidosChart =  DB::table('pedidos')
			->selectRaw(' count(*) as count ,  DATE_FORMAT(fecha_creado, \'%d\') as day , sum( pedidos.precio_total) as  amount ')
			->whereMonth('fecha_creado', 12)
			->groupBy('day')
			->get();

		$pedidosByDays = array();
		foreach ($pedidosChart as $pedidosTotal) {
			$pedidosByDays[$pedidosTotal->day] = ['count' => $pedidosTotal->count, 'amount' => $pedidosTotal->amount];
		}
/* 		dd($pedidosChart,  $pedidosChart->groupBy('day')->toArray(), $pedidosByDays);
 */		$productosChart = Producto::withCount('pedidos')->get();
		$productosByName = array();

		foreach ($productosChart as $producto) {
			$aux['name'] = $producto->nombre;
			$aux['count'] = $producto->pedidos->count();
			$productosByName[] =  $aux;
		}

		$productos = Producto::count();
		$recetas = Receta::count();
		$consultas = Contact::where('fecha_creado', '>=', $lastWeek)->count();
		$pedidos = Pedido::where('fecha_creado', '>=', $lastWeek)->count();
		return view('home.home', compact('productos', 'recetas', 'consultas', 'pedidos', 'productosByName', 'pedidosByDays'));
	}
}
