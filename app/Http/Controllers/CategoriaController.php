<?php

namespace App\Http\Controllers;

use App\Models\Categoria;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class CategoriaController extends Controller
{

    function __construct(){
        $this->middleware('permission:user-list|user-create|user-edit|user-delete', ['only' => ['index', 'store', 'filter']]);
        $this->middleware('permission:user-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }/**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = Categoria::sortable()
            ->orderBy('id', 'DESC')
            ->paginate(5);
        return view('category.index', compact('categorias'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = [
            'nombre' => 'required|min:4',
			'imagen' => 'required|image|mimes:jpg,png|max:512',
        ];
        $request->validate($rules);
		$imagePath = 'public/storage/';
		if ($request->hasFile('imagen') ) {

			$extension = $request->file('imagen')->extension();
			$fileOriginalName = pathinfo($request->file('imagen')->getClientOriginalName(), PATHINFO_FILENAME);
			$date = date('Y_m_d_H_i_s');
			$nameFile = $date . '_' . $fileOriginalName . '.' . $extension;
			$path = $request->file('imagen')->storeAs('categoria', $nameFile);
			$imagePath .= $path;
		}
        Categoria::create(array_merge($request->all(), ['imagen' => $imagePath] ) );
        return redirect()
            ->route('categorias.index')
            ->with('success', 'Se creo correctamente la categoria');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(403, 'Don`t have permission');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Categoria $categoria)
    {
        return  view('category.update', compact('categoria'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Categoria $categoria)
    {
        $rules = [
            'nombre' => 'required|min:4',
        ];
		$merged = [];
		if ($request->hasFile('imagen') ) {
			$imagePath = 'public/storage/';

			Storage::delete('categoria/'. pathinfo($categoria->imagen, PATHINFO_BASENAME)  );
			$extension = $request->file('imagen')->extension();
			$fileOriginalName = pathinfo($request->file('imagen')->getClientOriginalName(), PATHINFO_FILENAME);
			$date = date('Y_m_d_H_i_s');
			$nameFile = $date . '_' . $fileOriginalName . '.' . $extension;
			$path = $request->file('imagen')->storeAs('categoria', $nameFile);
			$imagePath .= $path;
			$merged['imagen'] = $imagePath;
		}
        $request->validate($rules);
        $categoria->update(array_merge($request->all(),$merged ) );
        return redirect()
            ->route('categorias.index')
            ->with('success', 'Se actualizó correctamente la categoria');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Categoria $categoria)
    {
		Storage::delete('categoria/'. pathinfo($categoria->imagen, PATHINFO_BASENAME)  );
        $categoria->delete();
        return redirect()
            ->route('categorias.index')
            ->with('success', 'Se elimino correctamente la categoria');
    }
    public function filter(Request $request){
        $categorias = Categoria::where('nombre', 'like', '%'.$request->nombre.'%')
            ->get();

        return view('category.index', compact('categorias') );
    }
}
