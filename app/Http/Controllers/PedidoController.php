<?php

namespace App\Http\Controllers;

use App\Models\Pedido;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\PedidoExport;
use App\Exports\PedidoItemExport;
use App\Exports\ProductoItemExport;
use App\Models\Estado;
use App\Models\TipoUsuario;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Session;

class PedidoController extends Controller
{

	function __construct()
	{
		$this->middleware('permission:order-list|order-create|order-edit|order-delete', ['only' => ['index', 'store', 'show', 'filter']]);
		$this->middleware('permission:order-create', ['only' => ['create', 'store']]);
		$this->middleware('permission:order-edit', ['only' => ['edit', 'update']]);
		$this->middleware('permission:order-delete', ['only' => ['destroy']]);
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		Session::forget('filter_pedidos');
		Session::forget('redirect');
		$pedidos = Pedido::sortable()
			->orderBy('fecha_modif', 'DESC')
			->paginate(5);
		$estados = Estado::all();
		$tiposUsuario = TipoUsuario::all();
		return view('order.index', compact('pedidos', 'estados', 'tiposUsuario'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show(Pedido $pedido)
	{
		return view('order.show', compact('pedido'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Pedido $pedido)
	{
		$estados = Estado::all();
		Session::put('redirect', Session::get('_previous')['url']);

		return view('order.update',  compact('pedido', 'estados'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request, Pedido $pedido)
	{
		$pedido->update($request->all());
		return redirect()->away(Session::get('redirect'))
			->with('success', 'Se actualizo correctamente el estado.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
	public function filter(Request $request)
	{
		Session::put('_previous', ['url' => route('pedidos.filter.get')]);
		$estados = Estado::all();
		$tiposUsuario = TipoUsuario::all();
		$pedidos = [];

		if (Session::has('filter_pedidos') && $request->nombre == null) {
			$pedidos = Session::get('filter_pedidos');
		} else if ($request->nombre) {
			$pedidos = Pedido::whereHas('usuario', function ($query) use ($request) {
				$names = explode(' ', strtolower(trim($request->nombre)));
				$query->whereIn('nombre', $names)
					->orWhereIn('apellido', $names)
					->orWhereIn('email', $names);
			})
				->orWhereHas('estadoR', function (Builder $query) use ($request) {
					$query->where('descripcion', 'like', '%' . $request->nombre . '%');
				})
				->orderBy('fecha_modif', 'DESC')
				->get();
			Session::put('filter_pedidos', $pedidos);
		} else {
			return redirect()->route('pedidos.index');
		}



		return view('order.index', compact('pedidos', 'estados', 'tiposUsuario'));
	}
	public function export(Request $request, $days, $state, $userType)
	{
		/* $usuario = new UsuarioController();
		$usuario->loadAddress(); */
		return Excel::download(new PedidoExport($days, $state,$userType), 'pedidos.xlsx');
	}
	public function exportItem($id)
	{
		/* $usuario = new UsuarioController();
		$usuario->loadAddress(); */
		return Excel::download(new PedidoItemExport($id), 'pedido-' . $id . '.xlsx');
	}
}
