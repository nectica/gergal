<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMinimoRequest;
use App\Models\MinimoCompra;
use Illuminate\Http\Request;

class MinimoCompraController extends Controller
{

	public function __construct(){
		$this->middleware('permission:minimo-list|minimo-create|minimo-edit|minimo-delete', ['only' => ['index', 'create',]]);
		$this->middleware('permission:minimo-create', ['only' => ['create', 'store']]);
		$this->middleware('permission:minimo-edit', ['only' => ['edit', 'update']]);
		$this->middleware('permission:minimo-delete', ['only' => ['destroy']]);
	}
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $minimos = MinimoCompra::sortable()->paginate(5);
		return view('minimo.index', compact('minimos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		return view('minimo.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreMinimoRequest $request)
    {
		MinimoCompra::create($request->all());
		return redirect()
			->route('minimo.index')
			->with('success', 'Se ha creado el mínimo de compra exitosamente.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(MinimoCompra $minimo)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(MinimoCompra $minimo)
    {
        return view('minimo.update', compact('minimo'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StoreMinimoRequest $request,MinimoCompra $minimo)
    {
		$minimo->update($request->all());
		return redirect()
			->route('minimo.index')
			->with('success', 'Se ha actualizado correctamente el minimo de compra');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(MinimoCompra $minimo)
    {
		$minimo->delete();
		return redirect()
			->route('minimo.index')
			->with('success', 'Se ha borrado correctamente el minimo de compra');
    }
}
