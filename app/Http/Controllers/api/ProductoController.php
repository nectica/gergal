<?php

namespace App\Http\Controllers\api;

use App\Http\Controllers\Controller;
use App\Models\Producto;
use Illuminate\Http\Request;

class ProductoController extends Controller
{
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		//
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(Request $request)
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(Request $request)
	{
		$producto = Producto::find($request->id)->makeHidden('producto_precio_unitario');
		$values = $this->isPercent($request->all(), $producto);
		$producto->update($values);

		return response()->json(['data' => [$producto->attributesToArray()]]);
	}
	public function updateByBulk(Request $request)
	{
		$productos = Producto::all();
		$productos = $productos->makeHidden('producto_precio_unitario');
		if($request->category != 'all'){
			$productos = $productos->filter(function  ($value, $key) use($request) {
				return $value->categoria == $request->category;
			});
		}
		switch ($request->fields) {
			case 'all':
				$this->bothPriceByPercent($productos, $request->percent / 100);
				break;
			case 'precio_lista':
				$this->precioListaByPercent($productos, $request->percent / 100);
				break;
			case 'precio_revendedor':
				$this->precioRevendedorByPercent($productos, $request->percent / 100);
				break;

		}
		return response()->json(['status' => 'ok']);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy($id)
	{
		//
	}
	//helpers
	public function isPercent($parameters, $producto)
	{
		if (isset($parameters['precio_revendedor']) && str_contains($parameters['precio_revendedor'], '%')) {
			$percent = intval($parameters['precio_revendedor']);
			$parameters['precio_revendedor'] = substr($producto->precio_lista, 1) * (1 -  ($percent / 100));
		}

		return $parameters;
	}
	public function bothPriceByPercent($productos, $percent)
	{
		foreach ($productos as $producto) {
			$producto->precio_lista = (float)substr($producto->precio_lista, 1) * (1 + ($percent ));
			$producto->precio_revendedor = (float)substr($producto->precio_revendedor, 1) * (1 + ($percent ));
			$producto->update();
		}
		return $productos;
	}
	public function precioListaByPercent($productos, $percent)
	{
		foreach ($productos as $producto) {
			$producto->precio_lista = (float)substr($producto->precio_lista, 1) * (1 + ($percent ));
			$producto->update();
		}
		return $productos;
	}
	public function precioRevendedorByPercent($productos, $percent)
	{
		foreach ($productos as $producto) {
			$producto->precio_revendedor = (float)substr($producto->precio_revendedor, 1) * (1 + ($percent ));
			$producto->update();
		}
		return $productos;
	}
}
