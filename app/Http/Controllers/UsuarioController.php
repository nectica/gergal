<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreUsuarioRequest;
use App\Http\Requests\UpdateUsuarioRequest;
use App\Models\TipoUsuario;
use App\Models\Usuario;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class UsuarioController extends Controller
{
	function __construct()
	{
		$this->middleware('permission:user-list|user-create|user-edit|user-delete', ['only' => ['index', 'store', 'filter']]);
		$this->middleware('permission:user-create', ['only' => ['create', 'store']]);
		$this->middleware('permission:user-edit', ['only' => ['edit', 'update']]);
		$this->middleware('permission:user-delete', ['only' => ['destroy']]);
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$usuarios = Usuario::sortable()
			->orderBy('fecha_modif', 'Desc')
			->paginate(5);

		return view('usuario.index', compact('usuarios'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		$tipoUsuario = TipoUsuario::all();
		return view('usuario.create', compact('tipoUsuario'));
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(StoreUsuarioRequest $request)
	{
		$merged = [];
		$merged['password'] = 123456;
		Usuario::create(array_merge($request->all(), $merged));
		return redirect()
			->route('usuarios.index')
			->with('success', 'Se creo correctamente el usuario');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show(Usuario $usuario)
	{
		return view('usuario.show', compact('usuario'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Usuario $usuario)
	{
		$tipoUsuario = TipoUsuario::all();
		return view('usuario.update', compact('usuario', 'tipoUsuario'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(UpdateUsuarioRequest $request, Usuario $usuario)
	{
		if (asset($request->password)) {
			unset($request['password']);
		}
		$usuario->update($request->all());
		return redirect()
			->route('usuarios.index')
			->with('success', 'Se ha actualizado correctamente el usuario.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Usuario $usuario)
	{
		$usuario->delete();
		return redirect()
			->route('usuarios.index')
			->with('success', 'Se ha borrado correctamente el usuario.');
	}
	public function filter(Request $request)
	{
		$usuarios =  Usuario::sortable()->where('nombre', 'like', '%' . $request->nombre . '%')
			->orWhere('apellido', 'like', '%' . $request->nombre . '%')
			->orWhere('email', 'like', '%' . $request->nombre . '%')
			->get();

		return view('usuario.index', compact('usuarios'));
	}
	/* helper */
	public function loadAddress()
	{

		$googleKey = 'AIzaSyDVY3YmApJPaBZqmG627e9XUx_5n3f502o.';
		$url = 'https://maps.googleapis.com/maps/api/geocode/json?latlng=';
		$usuarios = Usuario::where('latitud', '!=', 0)->where('longitud', '!=', 0)
		->where('calle_nro', null )
		->where('nombre_calle', null)
		->where('localidad', null)
		->where('partido', null)
		->where('provincia', null)
		->where('pais', null)
		->where('codigo_postal', null)
		->get();
		foreach ($usuarios as $usuario){
			$googleUrl = $url.$usuario->latitud.','.$usuario->longitud.'&key='.$googleKey;
			$http = Http::get($googleUrl);
			$json = $http->json();
			dd(	$googleUrl, $json);
			$addresses = $json['results'][0]['address_components'];
			foreach ($addresses as $address) {
				switch ($address['types'][0]) {
					case 'street_number':
						$usuario->calle_nro = $address['long_name'];
						break;
					case 'route':
					case 'establishment':
						$usuario->nombre_calle = $address['long_name'];
						break;
					case 'locality':
						$usuario->localidad = $address['long_name'];
						break;
					case 'administrative_area_level_1':
						$usuario->provincia = $address['short_name'];
						break;
					case 'administrative_area_level_2':
						$usuario->partido = $address['long_name'];
						break;
					case 'country':
						$usuario->pais = $address['long_name'];
						break;
					case 'postal_code':
						$usuario->codigo_postal = $address['long_name'];
						break;
					case 'postal_code_suffix':

						break;
				}
			}
			$usuario->update();
			sleep(1);
		}
	}
}
