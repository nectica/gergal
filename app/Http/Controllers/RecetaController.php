<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRecetaRequest;
use App\Http\Requests\UpdateRecetaRequest;
use App\Models\Receta;
use Illuminate\Support\Facades\Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class RecetaController extends Controller
{
	public function __construct()
	{
		$this->middleware('permission:recipe-list|recipe-create|recipe-edit|recipe-delete', ['only' => ['index', 'store', 'show', 'filter']]);
		$this->middleware('permission:recipe-create', ['only' => ['create', 'store']]);
		$this->middleware('permission:recipe-edit', ['only' => ['edit', 'update']]);
		$this->middleware('permission:recipe-delete', ['only' => ['destroy']]);
	}
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function index()
	{
		$recetas = Receta::sortable()
			->orderby('fecha_modif', 'desc')
			->paginate(5);
		return view('recipe.index', compact('recetas'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\Http\Response
	 */
	public function create()
	{
		return view('recipe.create');
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @return \Illuminate\Http\Response
	 */
	public function store(StoreRecetaRequest $request)
	{
		$imagePath = 'public/storage/';
		$merged = [];
		if ($request->hasFile('img')) {
			$extension = $request->file('img')->extension();
			$fileOriginalName = pathinfo($request->file('img')->getClientOriginalName(), PATHINFO_FILENAME);
			$date = date('Y_m_d_H_i_s');
			$nameFile = $date . '_' . $fileOriginalName . '.' . $extension;
			$path = $request->file('img')->storeAs('receta', $nameFile);
			$imagePath .= $path;
			$merged['image_path'] = $imagePath;
		}
		Receta::create(array_merge($request->all(), $merged));
		return redirect()
			->route('recetas.index')
			->with('success', 'Se ha creado correctamente la receta.');
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function show(Receta $receta)
	{
		return view('recipe.show', compact('receta'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function edit(Receta $receta)
	{
		return view('recipe.update', compact('receta'));
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function update(UpdateRecetaRequest $request, Receta $receta)
	{
		$imagePath = 'public/storage/';
		$merged = [];
		if ($request->hasFile('img')) {
			$imgMain = pathinfo($receta->image_path, PATHINFO_BASENAME);
			Storage::delete('receta' . $imgMain);/* aqui quede  */
			$extension = $request->file('img')->extension();
			$fileOriginalName = pathinfo($request->file('img')->getClientOriginalName(), PATHINFO_FILENAME);
			$date = date('Y_m_d_H_i_s');
			$nameFile = $date . '_' . $fileOriginalName . '.' . $extension;
			$path = $request->file('img')->storeAs('receta', $nameFile);
			$imagePath .= $path;
			$merged['image_path'] = $imagePath;
		}
		$receta->update(array_merge($request->all(), $merged));
		return redirect()
			->route('recetas.index')
			->with('success', 'Se ha actualizado correctamente la receta.');
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return \Illuminate\Http\Response
	 */
	public function destroy(Receta $receta)
	{
		Storage::delete('receta' . $receta->image_path);
		$receta->delete();
		return redirect()
			->route('recetas.index')
			->with('success', 'Se ha borrado correctamente la receta.');
	}

	public function filter(Request $request)
	{
		/* Session::put('_previous', ['url' => route('productos.filter.get')]); */
		$recetas = [];
		if (Session::has('filter_recetas') && $request->nombre == null) {
			$recetas = Session::get('filter_recetas');
		} else if ($request->nombre) {
			$recetas = Receta::where('titulo', 'like', '%' . $request->nombre . '%')
			->orWhere('ingredientes', 'like', '%' . $request->nombre . '%')
			->orWhere('preparacion', 'like', '%' . $request->nombre . '%')
			->get();

			Session::put('filter_recetas', $recetas );
		} else {

			return redirect()->route('recetas.index');
		}

		return view('recipe.index', compact('recetas'));
	}
}
