<?php

namespace App\Exports;

use App\Models\Pedido;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
class PedidoItemExport implements FromView
{
    public $id = 0;
    public function __construct($id){
        $this->id = $id;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $pedido = Pedido::find($this->id);
        return view('order.excelItem', compact('pedido'));
    }
}
