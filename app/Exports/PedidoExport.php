<?php

namespace App\Exports;

use App\Models\Pedido;
use Illuminate\Contracts\View\View;
use Illuminate\Support\Carbon;
use Maatwebsite\Excel\Concerns\FromView;

class PedidoExport implements FromView
{
	public $days = 0, $state = 0, $userType ;
    public function __construct($days, $state, $userType){
        $this->days = $days;
		$this->state =  $state;
		$this->userType =  $userType;
    }
    /**
    * @return \Illuminate\Support\Collection
    */
    public function  view(): View
    {

		$date = Carbon::today()->subDay($this->days);
        $pedidos = Pedido::orderBy('fecha_modif', 'Desc')
		->where('fecha_creado', '>=', $date)
		->when($this->state > 0, function($query){
			return $query->where('estado',$this->state );
		})
		->when($this->userType > 0, function($query){

			return $query->whereHas('usuario', function($query){
				return $query->where('tipo_usuario_id', $this->userType);
			});
		})
		->get();
        return view('order.excel', compact('pedidos') );
    }
}
