window.onload =  function(){


	const formTable = document.querySelectorAll('.form-dblclick');
	for (const form of formTable) {
		form.addEventListener('dblclick', handlerClick)
	}
	function handlerClick(evt){
		const p = evt.target
		const name = p.getAttribute("name");
		const input = document.querySelector('#'+name);
		input.type = 'text';

		input.addEventListener('blur', updateField);
		input.addEventListener('keyup', handlerKeyup)
		const value = input.value;
		input.select();
		toggleElement( p);

	}

	function toggleElement(element, show = 'none') {
		element.style.display = show ;
	}
	function updateField(evt){
		const input = evt.target;
		let value = input.value;
		if(value){
			const id = input.id.slice(input.id.lastIndexOf('_')+1);
			const p = document.querySelector( `[name=${input.id}]`);
			const nameParameter = input.getAttribute('name');
			const formData = new FormData();
			if(value.indexOf('$') !== -1 ){
				value =  value.slice( value.indexOf('$') + 1 );
			}
			formData.append('id', id);
			formData.append(nameParameter, value);
			toggleElement(p, show= 'block');
			fetch(url, {
				method: 'POST',
				body: formData
			}).then( response => response.json() )
			.then( (json)=>{
				const { data }= json;
				input.value = data[0][nameParameter];
				p.innerText = '';
				p.appendChild(document.createTextNode(data[0][nameParameter]));
				updatePercent(data[0], 'percent_'+ id );
			} )
			.catch(error => {
				alert('Error. no se actualizo el campo.');
			});
			;
			input.type = 'hidden';

		}
	}


	function handlerKeyup(evt){
		if (evt.keyCode == 13) {
			updateField(evt)
		}
	}
	function updatePercent(data, name, values = ['precio_lista', 'precio_revendedor']){
		const span = document.querySelector(`#${name}`);
		const percent = ( (1 - data[ values[1] ].slice(1) / data[ values[0] ].slice(1) ) * 100 ).toFixed(2)
		span.innerText = ''  ;
		span.appendChild(document.createTextNode(percent) )    ;
	}
}
