@extends('layouts.layout')

@section('content')
    <div class="row justify-content-between">
        <div class="col-lg-4 offset-2 my-2">
            <h2>crear un nuevo usuario</h2>
        </div>
        <div class="col-lg-3 my-2">
            <a class="btn btn-primary" href="{{ route('users.index') }}"> Atras</a>
        </div>
    </div>

    @if (count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> ERROR:<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    {!! Form::open(['route' => 'users.store', 'method' => 'POST']) !!}
    <div class="row justify-content-center">
        <div class="col-xs-8 col-sm-8 col-md-8">
            <div class="form-group">
                <strong>Nombre:</strong>
                {!! Form::text('name', null, ['placeholder' => 'nombre', 'class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                <strong>Email:</strong>
                {!! Form::text('email', null, ['placeholder' => 'Email', 'class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                <strong>Contraseña:</strong>
                {!! Form::password('password', ['placeholder' => 'Contraseña', 'class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                <strong>Confirmar contraseña:</strong>
                {!! Form::password('confirm-password', ['placeholder' => 'Confirmar contraseña', 'class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                <strong>Roles:</strong>
                {!! Form::select('roles[]', $roles, [], ['class' => 'form-control', 'multiple']) !!}
            </div>
            <button type="submit" class="btn btn-primary">Guardar</button>
        </div>

    </div>
    {!! Form::close() !!}



@endsection
