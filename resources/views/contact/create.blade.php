@extends('form.index')
@section('title', 'Crear Usuario.')
@section('title-form', 'Crear usuario.')
@section('route-form', route('usuarios.store'))
@section('content-form')

    <div class="form-group col-lg-4 pl-0">
		<label for="email">email</label>
		<input type="email" class="form-control" id="email" name="email" aria-describedby='title-feedback'
			value="{{ old('email') }}" >
		@error('email')
			<div id="title-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>

	<button type="submit" class="btn btn-success mb-1">Crear usuario</button>
@endsection
