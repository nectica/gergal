@extends('form.index')
@section('title', 'Actualizar receta.')
@section('title-form', 'Actualizar receta.')
@section('route-form', route('consultas.update', $contacto->id))
@section('content-form')
	@method('PUT')

	<div class="form-group col-lg-4 pl-0">
		<label for="email">email</label>
		<input type="email" class="form-control" id="email" name="email" aria-describedby='title-feedback'
			value="{{ old('email', $contacto->email) }}">
		@error('email')
			<div id="title-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>

	<button type="submit" class="btn btn-success mb-1">Actualizar contacto</button>
@endsection
