@extends('layouts.layout')
@section('title', $contacto->nombre)
@section('content')


	<div class="row justify-content-center">
		<div class="col-8 ">
			<p class="lead">
				Email: {{  $contacto->email }}
			</p>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-8 ">
			<p class="lead">
				Remitente: {{ $contacto->remitente }}
			</p>
		</div>
    </div>
    <div class="row justify-content-center">
		<div class="col-8 ">
			<p class="lead">
				Asunto: {{ $contacto->asunto }}
			</p>
		</div>
    </div>
    <div class="row justify-content-center">
		<div class="col-8 ">
			<p class="lead">
				Mensaje: {{ $contacto->mensaje }}
			</p>
		</div>
    </div>

@endsection
