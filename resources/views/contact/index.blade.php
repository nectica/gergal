@extends('layouts.layout')
@section('title', 'Pagina de contactos')
@section('content')
	<div class="row justify-content-center">
		@if ($message = Session::get('success'))
			<div class=" col-6  mt-2 alert alert-success">
				<p>{{ $message }}</p>
			</div>
		@endif

	</div>
	<div class="row justify-content-between m-3">
		<div class="col-lg-3 offset-1">
			<h2 class="font-color-gergal">contactos</h2>
		</div>
		<div class="col-lg-3 offset-1">
			<form action="{{ route('consultas.filter') }}" method="POST">
				@csrf
				<div class="input-group ">
					<input type="text" class="form-control"  name="nombre"
						placeholder="Filtrar por nombre, apellido o email" required>
					<div class="input-group-append">
						<button class="btn btn-gray " type="submit">Filtrar</button>
					</div>
				</div>
			</form>
        </div>
        <div class="col-lg-2"></div>

	</div>
	<div class="row justify-content-center">
		<div class="col-12 col-xl-10 table-responsive">
            @if ($contactos->isEmpty() )
                <p class="lead">No Hay consultas creadas.</p>
            @else
                <table class="table table-bordered">
                    <thead>
						<th scope="col">@sortablelink('email', 'Correo electrónico')</th>
						<th scope="col">@sortablelink('remitente', 'Remitente')</th>
						<th scope="col">@sortablelink('asunto', 'Asunto')</th>
                        <th scope="col"></th>
                    </thead>
                    <tbody>
                        @foreach ($contactos as $contacto)
                            <tr>
                                <td> <a href="{{ route('consultas.show', $contacto->id) }}">{{ $contacto->email }}</a> </td>
                                <td>{{ $contacto->remitente }}</td>
                                <td>{{ $contacto->asunto }}</td>


                                <td>
                                    @can('contact-edit')
                                        <a href="{{ route('consultas.edit', $contacto->id) }}" type="button"
                                            class="btn btn-outline-primary btn-sm  my-1" style="pointer-events: none;" > <i class="fas fa-edit"></i></a>
                                    @endcan
                                    @can('contact-delete')
                                        <form name="form-delete" action="{{ route('consultas.destroy', $contacto->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" style="pointer-events: none;" class="btn btn-outline-danger btn-sm" disabled><i class="fas fa-trash-alt"></i></button>
                                        </form>
                                    @endcan

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
            @if (  method_exists ($contactos, 'render')  )
                {{  $contactos->appends(\Request::except('page'))->render()  }}
            @endif
		</div>

	</div>


@endsection
<script src="{{ asset('js/delete.js') }}" defer></script>
