@extends('form.index')
@section('title', 'Crear receta.')
@section('title-form', 'Crear receta.')
@section('route-form', route('recetas.store'))
@section('content-form')
	<div class="form-group col-lg-4 pl-0">
		<label for="titulo">Titulo</label>
		<input type="text" class="form-control" id="titulo" name="titulo" aria-describedby='title-feedback'
			value="{{ old('titulo') }}">
		@error('titulo')
			<div id="title-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
	<div class="form-group  col-lg-8 pl-0">
		<label for="ingredientes">Ingredientes</label>
		<textarea class="form-control" id="ingredientes" name="ingredientes" aria-describedby='details-feedback'
			rows="3">{{ old('ingredientes') }}</textarea>
		@error('ingredientes')
			<div id="details-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
	<div class="form-group  col-lg-8 pl-0">
		<label for="preparacion">Preparaciòn</label>
		<textarea class="form-control" id="preparacion" name="preparacion" aria-describedby='details-feedback'
			rows="3">{{ old('preparacion') }}</textarea>
		@error('preparacion')
			<div id="details-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
	<div class="form-group col-lg-8 pl-0">
		<label for="img">Imagen de la receta</label>
		<input type="file" class="form-control-file" id="img" name="img">
		@error('img')
			<div id="title-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
	<button type="submit" class="btn btn-success mb-1">Crear recetas</button>
@endsection
