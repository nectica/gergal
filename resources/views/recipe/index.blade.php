@extends('layouts.layout')
@section('title', 'Pagina de recetas')
@section('content')
	<div class="row justify-content-center">
		@if ($message = Session::get('success'))
			<div class=" col-6  mt-2 alert alert-success">
				<p>{{ $message }}</p>
			</div>
		@endif

	</div>
	<div class="row justify-content-between m-3">
		<div class="col-lg-3 offset-1">
			<h2 class="font-color-gergal">Recetas</h2>
		</div>
		<div class="col-lg-3 offset-3">
			<form action="{{ route('recetas.filter') }}" method="POST">
				@csrf
				<div class="input-group ">
					<input type="text" class="form-control"  name="nombre"
						placeholder="Filtrar por nombre, ingredientes" required>
					<div class="input-group-append">
						<button class="btn btn-gray " type="submit">Filtrar</button>
					</div>
				</div>
			</form>
		</div>
		@can('recipe-create')
			<div class="col-lg-2">
				<a href="{{ route('recetas.create') }}" type="button" class="btn btn-success-gergal">Crear receta</a>
			</div>
		@endcan
	</div>
	<div class="row justify-content-center">
		<div class="col-12 col-xl-10 table-responsive">
            @if ($recetas->isEmpty())
                <p class="lead">No hay recetas registrados</p>
            @else
                <table class="table table-bordered">
                    <thead>
                        @if ( !method_exists($recetas, 'render') )
                            <th scope="col">Titulo</th>
                            <th scope="col">Ingredientes</th>
                            <th scope="col">Preparacion</th>
                            <th scope="col">imagen</th>
                        @else
                            <th scope="col">@sortablelink('titulo', 'Titulo')</th>
                            <th scope="col">@sortablelink('ingredientes', 'Ingredientes')</th>
                            <th scope="col">@sortablelink('preparacion', 'Preparacion')</th>
                            <th scope="col">@sortablelink('image_path', 'imagen')</th>
                        @endif
                        <th scope="col"></th>
                    </thead>
                    <tbody>
                        @foreach ($recetas as $receta)
                            <tr>
                                <td> <a href="{{ route('recetas.show', $receta->id) }}">{{ $receta->titulo }}</a> </td>
                                <td>{{ $receta->ingredientes }}</td>
                                <td>{{ $receta->preparacion }}</td>
                                <td><img src="{{  asset(substr($receta->image_path, 6) )   }}" alt="recetas"  width="80" height="80"> </td>


                                <td>
                                    @can('recipe-edit')
                                        <a href="{{ route('recetas.edit', $receta->id) }}" type="button"
                                            class="btn btn-outline-primary btn-sm  my-1"> <i class="fas fa-edit"></i></a>
                                    @endcan
                                    @can('recipe-delete')
                                        <form name="form-delete" action="{{ route('recetas.destroy', $receta->id) }}" method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-outline-danger btn-sm" disabled><i class="fas fa-trash-alt"></i></button>
                                        </form>
                                    @endcan

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
            {{ method_exists($recetas, 'appends') ? $recetas->appends(\Request::except('page'))->render() : '' }}

		</div>

	</div>


@endsection
<script src="{{ asset('js/delete.js') }}" defer></script>
