@extends('layouts.layout')
@section('title', $receta->titulo)
@section('content')
	<div class="row  m-3">
		<div class="col-4 offset-1">
			<h2>{{ $receta->titulo }}</h2>
		</div>

	</div>
	<div class="row justify-content-center mb-5 px-2">
		<div class="col-lg-4 ">
			<div class="text-center">
				<img src="{{ asset(substr($receta->image_path, 6)) }}" class="img-fluid"
					alt="{{ $receta->nombre }}">
			</div>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-8 ">
			<p class="lead">
				Ingredientes: {{  $receta->ingredientes }}
			</p>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-8 ">
			<p class="lead">
				Preparacion: {{ $receta->preparacion }}
			</p>
		</div>
	</div>
@endsection
