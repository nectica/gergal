@extends('form.index')
@section('title', 'Crear mínimo de compra.')
@section('title-form', 'Crear mínimo de compra.')
@section('route-form', route('minimo.store'))
@section('content-form')
	<div class="form-group col-lg-4 pl-0">
		<label for="monto_minimo_compra">Mínimo de compra cliente</label>
		<input type="text" class="form-control" id="monto_minimo_compra" name="monto_minimo_compra" aria-describedby='monto_minimo_compra-feedback'
			value="{{ old('monto_minimo_compra') }}">
		@error('monto_minimo_compra')
			<div id="monto_minimo_compra-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
	<div class="form-group col-lg-4 pl-0">
		<label for="minimo_compra_revendedor">Mínimo de compra cliente</label>
		<input type="text" class="form-control" id="minimo_compra_revendedor" name="minimo_compra_revendedor" aria-describedby='minimo_compra_revendedor-feedback'
			value="{{ old('minimo_compra_revendedor') }}">
		@error('minimo_compra_revendedor')
			<div id="minimo_compra_revendedor-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>

	<button type="submit" class="btn btn-success mb-1">Crear Mínimo</button>
@endsection
