@extends('layouts.layout')
@section('title', 'Mínimo de compras')
@section('content')
    <div class="row justify-content-center">
        @if ($message = Session::get('success'))
            <div class=" col-6  mt-2 alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif

    </div>
    <div class="row justify-content-between m-3">
        <div class="col-lg-3 offset-1">
            <h2 class="font-color-gergal">Mínimo de compras</h2>
        </div>
        <div class="col-lg-3 offset-3">
            <form action="{{ route('minimo.filter') }}" method="POST">
                @csrf
                <div class="input-group ">
                    <input type="text" class="form-control" name="nombre" placeholder="Filtrar por nombre">
                    <div class="input-group-append">
                        <button class="btn btn-gray " type="submit">Filtrar</button>
                    </div>
                </div>
            </form>
        </div>
        @can('minimo-create')
            <div class="col-lg-2">
                <a href="{{ route('minimo.create') }}" style="font-size: 14px;" type="button"
                    class="btn btn-success-gergal">Crear mínimo de compra</a>
            </div>
        @endcan
    </div>
    <div class="row justify-content-center">
        <div class="col-12 col-xl-10 table-responsive">
            @if ($minimos->isEmpty())
                <p class="lead">No existen mínimo de compras registrados</p>
            @else
                <table class="table table-bordered">
                    <thead>
                        <th>N°</th>
                        <th>@sortablelink('monto_minimo_compra', 'Mínimo del cliente') </th>
                        <th>@sortablelink('minimo_compra_revendedor', 'Mínimo del revendedor') </th>
                        <th scope="col"></th>
                    </thead>
                    <tbody>
                        @foreach ($minimos as $index => $minimo)
                            <tr>
                                <td class="text-gergal-color">{{ $index + 1 }}</td>
                                <td>{{ $minimo->monto_minimo_compra }}</td>
                                <td>{{ $minimo->minimo_compra_revendedor }}</td>
                                <td>
                                    @can('minimo-edit')
                                        <a href="{{ route('minimo.edit', $minimo->id) }}" type="button"
                                            class="btn btn-outline-primary btn-sm  my-1"> <i class="fas fa-edit"></i></a>
                                    @endcan
                                    @can('minimo-delete')
                                        <form name="form-delete" action="{{ route('minimo.destroy', $minimo->id) }}"
                                            method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-outline-danger btn-sm" disabled><i
                                                    class="fas fa-trash-alt"></i></button>
                                        </form>
                                    @endcan

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>

            @endif
            {{ method_exists($minimos, 'appends') ? $minimos->appends(\Request::except('page'))->render() : '' }}

        </div>

    </div>


@endsection
<script src="{{ asset('js/delete.js') }}" defer></script>
