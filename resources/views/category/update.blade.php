@extends('form.index')
@section('title', 'Actualizar categoria.')
@section('title-form', 'Actualizar categoria.')
@section('route-form', route('categorias.update',$categoria->id))
@section('content-form')
@method('PUT')
	<div class="form-group col-lg-4 pl-0">
		<label for="title">Nombre</label>
		<input type="text" class="form-control" id="nombre" name="nombre" aria-describedby='title-feedback'
			value="{{ old('nombre', $categoria->nombre) }}">
		@error('nombre')
			<div id="title-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
	<div class="form-group col-lg-8 pl-0">
		<label for="imagen">Imagen de la categoria</label>
		<input type="file" class="form-control-file" id="imagen" name="imagen">
		@error('imagen')
			<div id="imagen-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
	<button type="submit" class="btn btn-success mb-1">Actualizar categoria</button>
@endsection
