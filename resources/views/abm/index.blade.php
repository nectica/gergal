@extends('layouts.layout')
@section('title', $title)
@section('content')
    <div class="row justify-content-center">
        @if ($message = Session::get('success'))
            <div class=" col-6  mt-2 alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif

    </div>
    <div class="row justify-content-between m-3">
        <div class="col-lg-3 offset-1">
            <h2 class="font-color-gergal">{{$title}}</h2>
        </div>
        <div class="col-lg-3 offset-3">
            <form action="{{ route($routeFilter) }}" method="POST">
                @csrf
                <div class="input-group ">
                    <input type="text" class="form-control" id="searchInput" name="title"
                        placeholder="Filtrar por nombre o descripción">
                    <div class="input-group-append">
                        <button class="btn btn-gray " type="submit">Filtrar</button>
                    </div>
                </div>
            </form>
        </div>
        @can($rolCreate)
            <div class="col-lg-2">
                <a href="{{ route($routerCreate) }}" type="button" class="btn btn-success-gergal">Crear Noticia</a>
            </div>
        @endcan
    </div>
    <div class="row justify-content-center">
        <div class="col-12 col-xl-10 table-responsive">
            <table class="table table-bordered">
                <thead>
                    @foreach ($theads as $th)
                    <th scope="col">@sortablelink($th->field, $th->label)</th>

                    @endforeach
                </thead>
                <tbody>
                    @foreach ($model as $value)
                        <tr>
                            <td><a href="{{ route($routeShow, $value->id) }}">{{ $value }}</a></td>
                            @foreach ($theads as $th)

                            @endforeach
                            <td>{{ strlen($newsItem->details) > 255 ? substr($newsItem->details, 0, 255) . '...' : $newsItem->details }}
                            </td>
                            <td>{{ $newsItem->image }}</td>
                            <td>{{ $newsItem->executive == 1 ? 'Si' : 'No' }}</td>
                            <td>{{ $newsItem->patner == 1 ? 'Si' : 'No' }}</td>
                            <td>{{ $newsItem->destacado == 1 ? 'Si' : 'No' }}</td>
                            <td>
                                @can('news-edit')
                                    <a href="{{ route('news.edit', $newsItem->id) }}" type="button"
                                        class="btn btn-outline-primary btn-sm  my-1"> <i class="fas fa-edit"></i></a>
                                @endcan
                                @can('news-delete')
                                    <form name="form-delete" action="{{ route('news.delete', $newsItem->id) }}" method="POST">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-outline-danger btn-sm" disabled><i class="fas fa-trash-alt"></i></button>
                                    </form>
                                @endcan

                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
            {{method_exists($news, 'appends') ? $news->appends(\Request::except('page'))->render() : '' }}

        </div>

    </div>


@endsection
<script src="{{ asset('js/delete.js') }}" defer></script>
