@extends('layouts.layout')
@section('title', $usuario->nombre)
@section('content')
	<div class="row  m-3">
		<div class="col-4 offset-1">
			<h2>{{  $usuario->nombre .' '.$usuario->apellido }}</h2>
		</div>

	</div>

	<div class="row justify-content-center">
		<div class="col-8 ">
			<p class="lead">
				Email: {{  $usuario->email }}
			</p>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-8 ">
			<p class="lead">
				Teléfono: {{ $usuario->telefono }}
			</p>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-8 ">
			<p class="lead">
				Dirección: {{ $usuario->direccion }}
			</p>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-8 ">
			<p class="lead">
				Latitud: {{ $usuario->latitud }}
			</p>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-8 ">
			<p class="lead">
				Longitud: {{ $usuario->longitud }}
			</p>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-8 ">
			<p class="lead">
				Tipo de usuario: {{ $usuario->tipoUsuario->descripcion }}
			</p>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-8 ">
			<p class="lead">
				Activo: {{ $usuario->estado ? 'Si' : 'No' }}
			</p>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-8 ">
			<div class="accordion" >
				<div class="card">
					<div class="card-header" >
					  <h2 class="mb-0" >
						<button id="accordionParent" onclick=" return collapse();" class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
							PEDIDOS
						</button>
					  </h2>
					</div>
					<div  class="collapse" aria-labelledby="headingOne" data-parent="accordionParent" >
						@if ($usuario->pedidos->isEmpty())
							<p class="lead pl-3">Sin pedidos</p>

						@else
							<div class="accordion ml-3" id="accordionExample">
								@foreach ($usuario->pedidos as $pedido)

								<div class="card">
								<div class="card-header" >
									<h2 class="mb-0" >

									<button id="{{$pedido->id}}" onclick=" return collapse();" class="btn btn-link btn-block text-left" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
										pedido: {{ $pedido->id }}
									</button>
									</h2>
								</div>

								<div id="collapse{{$pedido->id}}" class="collapse" aria-labelledby="headingOne" data-parent="{{$pedido->id}}" >
									<div class="card-body table-responsive">
                                        @include('order.order')
									</div>
								</div>
								</div>

								@endforeach
							</div>
                        @endif
					</div>
				</div>
			</div>

		</div>
	</div>
<script>
	function collapse(){
		const evt = arguments.callee.caller.arguments[0];
		const body =  document.querySelector(`[data-parent='${evt.target.id}']`);
		body.classList.toggle('show');

	}
</script>
@endsection
