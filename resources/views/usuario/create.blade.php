@extends('form.index')
@section('title', 'Crear Usuario.')
@section('title-form', 'Crear usuario.')
@section('route-form', route('usuarios.store'))
@section('content-form')
    <div class="form-row">
        <div class="form-group col-lg-4 ">
            <label for="nombre">Nombre</label>
            <input type="text" class="form-control" id="nombre" name="nombre" aria-describedby='title-feedback'
                value="{{ old('nombre') }}">
            @error('nombre')
                <div id="title-feedback" class="is-invalid ">
                    <small class="text-danger">*{{ $message }}</small>
                </div>
            @enderror
        </div>
        <div class="form-group col-lg-4 pl-0">
            <label for="apellido">Apellido</label>
            <input type="text" class="form-control" id="apellido" name="apellido" aria-describedby='title-feedback'
                value="{{ old('apellido') }}">
            @error('apellido')
                <div id="title-feedback" class="is-invalid ">
                    <small class="text-danger">*{{ $message }}</small>
                </div>
            @enderror
        </div>
    </div>
    <div class="form-group col-lg-4 pl-0">
		<label for="email">email</label>
		<input type="email" class="form-control" id="email" name="email" aria-describedby='title-feedback'
			value="{{ old('email') }}" >
		@error('email')
			<div id="title-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
    <div class="form-group col-lg-4 pl-0">
		<label for="telefono">telefono</label>
		<input type="text" class="form-control" id="telefono" name="telefono" aria-describedby='title-feedback'
			value="{{ old('telefono') }}" >
		@error('telefono')
			<div id="title-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
	{{-- <div class="form-group col-lg-4 pl-0">
		<label for="password">Contraseña</label>
		<input type="password" class="form-control" id="password" name="password" aria-describedby='title-feedback'
			value="{{ old('password') }}" >
		@error('password')
			<div id="title-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
	<div class="form-group col-lg-4 pl-0">
		<label for="password_confirmation">repetir Contraseña</label>
		<input type="password" class="form-control" id="password_confirmation" name="password_confirmation" aria-describedby='title-feedback'
			value="{{ old('password_confirmation') }}" >
		@error('password_confirmation')
			<div id="title-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div> --}}

    <div class="form-group col-lg-4 pl-0">
		<label for="email">Tipo de usuario</label>

        <select name="tipo_usuario_id" id="tipo_usuario_id" class="form-control" required>
            <option selected disabled  >Selecciona tipo de usuario</option>
            @foreach ($tipoUsuario as $tipo)
                <option value="{{$tipo->id}}" {{ old('tipo_usuario_id') == $tipo->id ? 'selected' : '' }}>{{$tipo->descripcion}}</option>
            @endforeach
        </select>
    </div>
    <div class="form-group col-lg-8 pl-0">
		<div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="estado" value="1" {{ old('estado') ==1 ? 'checked' : '' }} name="estado">
			<label class="custom-control-label" for="estado">Usuario Activo</label>
		</div>
	</div>
	<button type="submit" class="btn btn-success mb-1">Crear usuario</button>
@endsection
