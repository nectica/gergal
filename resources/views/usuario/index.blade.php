@extends('layouts.layout')
@section('title', 'Pagina de Usuarios')
@section('content')
	<div class="row justify-content-center">
		@if ($message = Session::get('success'))
			<div class=" col-6  mt-2 alert alert-success">
				<p>{{ $message }}</p>
			</div>
		@endif

	</div>
	<div class="row justify-content-between m-3">
		<div class="col-lg-3 offset-1">
			<h2 class="font-color-gergal">Usuarios</h2>
		</div>
		<div class="col-lg-3 offset-3">
			<form action="{{ route('usuarios.filter') }}" method="POST">
				@csrf
				<div class="input-group ">
					<input type="text" class="form-control"  name="nombre"
						placeholder="Filtrar por nombre, apellido o email" required>
					<div class="input-group-append">
						<button class="btn btn-gray " type="submit">Filtrar</button>
					</div>
				</div>
			</form>
		</div>
		@can('user-create')
			<div class="col-lg-2">
				<a href="{{ route('usuarios.create') }}" type="button" class="btn btn-success-gergal">Crear usuario</a>
			</div>
		@endcan
	</div>
	<div class="row justify-content-center">
		<div class="col-12 col-xl-10 table-responsive">
			@if ($usuarios->isEmpty())
				<p class="lead">No se tiene usuarios registrados.</p>
			@else
				<table class="table table-bordered">
					<thead>
						@if ( !method_exists($usuarios, 'render') )
							<th scope="col">Correo electronico</th>
							<th scope="col">nombre</th>
							<th scope="col">apellido</th>
							<th scope="col">Tipo de usuario</th>
							<th scope="col">Activo</th>
						@else
							<th scope="col">@sortablelink('email', 'Correo electronico')</th>
							<th scope="col">@sortablelink('nombre', 'nombre')</th>
							<th scope="col">@sortablelink('apellido', 'apellido')</th>
							<th scope="col">@sortablelink('tipo_usuario_id', 'Tipo de usuario')</th>
							<th scope="col">@sortablelink('estado', 'Activo')</th>
						@endif
						<th scope="col"></th>
					</thead>
					<tbody>
						@foreach ($usuarios as $usuario)
							<tr>
								<td> <a href="{{ route('usuarios.show', $usuario->id) }}">{{ $usuario->email }}</a> </td>
								<td>{{ $usuario->nombre }}</td>
								<td>{{ $usuario->apellido }}</td>
								<td>{{ $usuario->tipoUsuario->descripcion }}</td>
								<td>{{ $usuario->estado ? 'Si' : 'No' }}</td>


								<td>
									@can('user-edit')
										<a href="{{ route('usuarios.edit', $usuario->id) }}" type="button"
											class="btn btn-outline-primary btn-sm  my-1"> <i class="fas fa-edit"></i></a>
									@endcan
									@can('user-delete')
										<form name="form-delete" action="{{ route('usuarios.destroy', $usuario->id) }}" method="POST">
											@csrf
											@method('DELETE')
											<button type="submit" class="btn btn-outline-danger btn-sm" disabled><i class="fas fa-trash-alt"></i></button>
										</form>
									@endcan

								</td>
							</tr>
						@endforeach
					</tbody>
				</table>
			@endif
            {{ method_exists($usuarios, 'appends') ? $usuarios->appends(\Request::except('page'))->render() : '' }}


		</div>

	</div>


@endsection
<script src="{{ asset('js/delete.js') }}" defer></script>
