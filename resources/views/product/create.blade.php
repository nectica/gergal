@extends('form.index')
@section('title', 'Crear Producto.')
@section('title-form', 'Crear Producto.')
@section('route-form', route('productos.store'))
@section('content-form')
	<div class="form-group col-lg-4 pl-0">
		<label for="nombre">Nombre</label>
		<input type="text" class="form-control" id="nombre" name="nombre" aria-describedby='title-feedback'
			value="{{ old('nombre') }}">
		@error('nombre')
			<div id="nombre-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
	<div class="form-group  col-lg-8 pl-0">
		<label for="descripcion">Descripción</label>
		<textarea class="form-control" id="descripcion" name="descripcion" aria-describedby='details-feedback'
			rows="3">{{ old('descripcion') }}</textarea>
		@error('descripcion')
			<div id="descripcion-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
	<div class="form-group col-lg-8 pl-0">
		<label for="propiedades">Propiedades</label>
		<textarea class="form-control" id="propiedades" name="propiedades" aria-describedby='details-feedback'
			rows="3">{{ old('propiedades') }}</textarea>
		@error('propiedades')
			<div id="propiedades-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
	<div class="form-group col-lg-8 pl-0">
		<label for="info_nutricional">Información nutricional</label>
		<textarea class="form-control" id="info_nutricional" name="info_nutricional" aria-describedby='details-feedback'
			rows="3">{{ old('info_nutricional') }}</textarea>
		@error('info_nutricional')
			<div id="info_nutricional-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
	<div class="form-group col-lg-8 pl-0">
		<label for="img">Imagen del producto</label>
		<input type="file" class="form-control-file" id="img" name="img">
		@error('img')
			<div id="img-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
	<div class="form-group col-lg-8 pl-0">
		<label for="img_hoover">Imagen descriptiva producto</label>
		<input type="file" class="form-control-file" id="img_hoover" name="img_hoover">
		@error('img_hoover')
			<div id="img_hoover-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
	<div class="form-group col-lg-4 pl-0">
		<label for="categoria">Categoria</label>
		<select class="form-control" name="categoria" id="categoria" >
			<option disabled selected>--- Seleccione una categoria ---</option>
			@foreach ($categorias as $categoria)
				<option value="{{ $categoria->id }}" {{ old('categoria') == $categoria->id ? 'selected' : '' }}>{{ $categoria->nombre }}</option>
			@endforeach
		</select>
		@error('sub_categoria')
			<div id="categoria-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
	<div class="form-group col-lg-4 pl-0">
		<label for="sub_categoria">Sub categoria</label>
		<select class="form-control" name="sub_categoria" id="sub_categoria" >
			<option disabled selected>--- Seleccione una sub categoria ---</option>
			@foreach ($categorias as $categoria)
				<option value="{{ $categoria->id }}" {{ old('sub_categoria') == $categoria->id ? 'selected' : '' }}>{{ $categoria->nombre }}</option>
			@endforeach
		</select>
		@error('sub_categoria')
			<div id="sub_categoria-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
	<div class="form-group col-lg-4 pl-0">
		<label for="precio_lista">Precio de lista</label>
		<input type="string" class="form-control" id="precio_lista" name="precio_lista" aria-describedby='title-feedback'
			value="{{ old('precio_lista') }}">
		@error('precio_lista')
			<div id="precio_lista-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
	<div class="form-group col-lg-4 pl-0">
		<label for="precio_revendedor">Precio de revendedor</label>
		<input type="string" class="form-control" id="precio_revendedor" name="precio_revendedor"
			aria-describedby='title-feedback' value="{{ old('precio_revendedor') }}">
		@error('precio_revendedor')
			<div id="precio_revendedor-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
	<div class="form-group col-lg-4 pl-0">
		<label for="cantidad_en_stock">Stock</label>
		<input type="text" class="form-control" id="cantidad_en_stock" name="cantidad_en_stock" aria-describedby='title-feedback'
			value="{{ old('cantidad_en_stock') }}" >
		@error('cantidad_en_stock')
			<div id="cantidad_en_stock-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
	<div class="form-group col-lg-4 pl-0">
		<label for="orden">Orden</label>
		<input type="text" class="form-control" id="orden" name="orden" aria-describedby='orden-feedback'
			value="{{ old('orden', 1000) }}" >
		@error('orden')
			<div id="orden-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
	<div class="form-group col-lg-8 pl-0">
		<div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="estado" value="1" {{ old('estado') ==1 ? 'checked' : '' }} name="estado">
			<label class="custom-control-label" for="estado">Listo para publicar para usuario</label>
		</div>
	</div>
	<div class="form-group col-lg-8 pl-0">
		<div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="estado_revendedores" value="1" {{ old('estado') ==1 ? 'checked' : '' }} name="estado_revendedores">
			<label class="custom-control-label" for="estado_revendedores">Listo para publicar para revendedor</label>
		</div>
	</div>
	<div class="form-group col-lg-8 pl-0">
		<div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="destacado" value="1" {{ old('destacado') ==1 ? 'checked' : '' }} name="destacado">
			<label class="custom-control-label" for="destacado">Destacado</label>
		</div>
	</div>
	<div class="form-group col-lg-8 pl-0">
		<div class="custom-control custom-switch">
            <input type="checkbox" class="custom-control-input" id="novedades" value="1" {{ old('novedades') ==1 ? 'checked' : '' }} name="novedades">
			<label class="custom-control-label" for="novedades">Novedades</label>
		</div>
	</div>
	<button type="submit" class="btn btn-success mb-1">Crear Producto</button>
@endsection
