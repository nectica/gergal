@extends('layouts.layout')
@section('title', $producto->nombre)
@section('content')
	<div class="row  m-3">
		<div class="col-4 offset-1">
			<h2>{{ $producto->nombre }}</h2>
		</div>

	</div>
	<div class="row justify-content-center mb-5 px-2">
		<div class="col-lg-4 img-effect">
			<div class="text-center">
				<img src="{{ asset(substr($producto->image_path, 6)) }}" class="img-fluid img-hover-hidden"
					alt="{{ $producto->nombre }}">
			</div>
		</div>
		<div class="col-lg-4 img-effect">
			<div class="text-center">
				<img src="{{ asset(substr($producto->hover_image_path, 6)) }}" class="img-fluid img-hover-show"
					alt="{{ $producto->nombre }}">
			</div>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-8 ">
			<p class="lead">
				Descripción: {{ $producto->descripcion }}
			</p>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-8 ">
			<p class="lead">
				Categoria: {{  empty($producto->categoria)   ? '-' : $producto->categoriaR->nombre }}
			</p>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-8 ">
			<p class="lead">
				Sub categoria: {{  empty($producto->subcategoria)   ? '-' : $producto->subcategoria->nombre }}
			</p>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-8 ">
			<p class="lead">
				Propiedades: {{ $producto->propiedades }}
			</p>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-8 ">
			<p class="lead">
				Información nutricional: {{ $producto->info_nutricional }}
			</p>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-8 ">
			<p class="lead">
				Precio de Lista: {{ $producto->precio_lista}}
			</p>
		</div>
	</div>
	<div class="row justify-content-center">
		<div class="col-8 ">
			<p class="lead">
				Precio de revendedor: {{ $producto->precio_revendedor }}
			</p>
		</div>
    </div>
    <div class="row justify-content-center">
		<div class="col-8 ">
			<p class="lead">
				Publicado: {{ $producto->estado ? 'Si' : 'No' }}
			</p>
		</div>
	</div>
@endsection
