@extends('layouts.layout')
@section('title', 'Pagina de productos')
@section('content')
    <div class="row justify-content-center">
        @if ($message = Session::get('success'))
            <div class=" col-6  mt-2 alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif

    </div>
    <div class="row justify-content-between m-3">
        <div class="col-lg-3 offset-1">
            <h2 class="font-color-gergal">Productos</h2>
        </div>
        <div class=" col-12 col-lg-2">
            <button style="    background-color: #02934b !important;" class="btn btn-success"
                title="Actualizar masivamente los precios del producto" id="modalOpen">Actualización masiva</button>
        </div>
        <div class="col-lg-3 ">
            <form action="{{ route('productos.filter') }}" method="POST">
                @csrf
                <div class="input-group ">
                    <input type="text" class="form-control" name="nombre" placeholder="Filtrar por nombre o descripción">
                    <div class="input-group-append">
                        <button class="btn btn-gray " type="submit">Filtrar</button>
                    </div>
                </div>
            </form>
        </div>
        @can('product-create')
            <div class="col-lg-2">
                <a href="{{ route('productos.create') }}" type="button" class="btn btn-success-gergal">Crear producto</a>
            </div>
        @endcan
    </div>
    <div class="row justify-content-center">
        <div class="col-12 col-xl-10 table-responsive">
            @if ($productos->isEmpty())
                <p class="lead">No Hay Productos registrados</p>
            @else
                <table class="table table-bordered">
                    <thead>

                        @if (method_exists($productos, 'appends'))
                            <th scope="col">@sortablelink('nombre', 'Nombre')</th>
                            <th scope="col">@sortablelink('descripcion', 'Descripción') </th>
                            <th scope="col">@sortablelink('categoria','Categoría') </th>
                            <th scope="col">@sortablelink('cantidad_en_stock', 'Stock')</th>
                            <th scope="col">@sortablelink('propiedades', 'Propiedades') </th>
                            <th scope="col">@sortablelink('precio_lista', 'Precio de lista') </th>
                            <th scope="col">@sortablelink('precio_revendedor', 'Precio de revendedor') </th>
                            <th scope="col">Precio diferencia</th>
                            <th scope="col">@sortablelink('estado', 'Publicado') </th>
                            <th scope="col"></th>

                        @else
                            <th scope="col">Nombre</th>
                            <th scope="col">Descripción </th>
                            <th scope="col">Categoría </th>
                            <th scope="col">Stock</th>
                            <th scope="col">Propiedades </th>
                            <th scope="col">Precio de lista </th>
                            <th scope="col">Precio de revendedor </th>
                            <th scope="col">Precio diferencia</th>
                            <th scope="col">Publicado</th>
                            <th scope="col"></th>
                        @endif

                    </thead>
                    <tbody>
                        @foreach ($productos as $producto)

                            <tr>
                                <td><a href="{{ route('productos.show', $producto->id) }}">{{ $producto->nombre }}</a>
                                </td>
                                <td>{{ strlen($producto->descripcion) > 120 ? substr($producto->descripcion, 0, 120) . '...' : $producto->descripcion }}
                                </td>
                                <td>{{ empty($producto->categoria) ? '-' : $producto->categoriaR->nombre }}</td>
                                <td>
                                    <p class="form-dblclick" name="{{ 'cantidad_en_stock_' . $producto->id }}">
                                        {{ $producto->cantidad_en_stock ?: '-' }}</p>
                                    <input class="form-control" id="{{ 'cantidad_en_stock_' . $producto->id }}"
                                        type="hidden" name="cantidad_en_stock" value={{ $producto->cantidad_en_stock }}>
                                </td>
                                <td>{{ $producto->propiedades > 120 ? substr($producto->propiedades, 0, 120) . '...' : $producto->propiedades }}
                                </td>
                                </td>
                                <td>

                                    <p class="form-dblclick" name="{{ 'precio_lista_' . $producto->id }}">
                                        {{ $producto->precio_lista }}</p>
                                    <input class="form-control" id="{{ 'precio_lista_' . $producto->id }}" type="hidden"
                                        name="precio_lista" value={{ $producto->precio_lista }}>
                                </td>
                                <td>
                                    <p class="form-dblclick" name="{{ 'precio_revendedor_' . $producto->id }}">
                                        {{ $producto->precio_revendedor }}</p>
                                    <input class="form-control" id="{{ 'precio_revendedor_' . $producto->id }}"
                                        type="hidden" name="precio_revendedor" value={{ $producto->precio_revendedor }}>
                                </td>
                                <td> <span
                                        id="{{ 'percent_' . $producto->id }}">{{ round((1 - (int)substr($producto->precio_revendedor, 1) / (int)substr($producto->precio_lista, 1)) * 100, 2) }}
                                    </span> %</td>
                                <td>{{ $producto->estado ? 'Si' : 'No' }}</td>
                                <td>
                                    @can('product-edit')
                                        <a href="{{ route('productos.edit', $producto->id) }}" type="button"
                                            class="btn btn-outline-primary btn-sm  my-1"> <i class="fas fa-edit"></i></a>
                                    @endcan
                                    {{-- @can('product-delete')
                                        <form name="form-delete" action="{{ route('productos.destroy', $producto->id) }}"
                                            method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-outline-danger btn-sm" disabled><i
                                                    class="fas fa-trash-alt"></i></button>
                                        </form>
                                    @endcan --}}

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
            {{ method_exists($productos, 'appends') ? $productos->appends(\Request::except('page'))->render() : '' }}
        </div>

    </div>

    <div class="modal" id="modal" tabindex="-1">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group col-lg-4 pl-0">
                        <label for="percent">Porcentaje de aumento (%)</label>
                        <input type="number" max="100" class="form-control" id="percent" name="percent"
                            aria-describedby='title-feedback'>
                        @error('percent')
                            <div id="percent-feedback" class="is-invalid ">
                                <small class="text-danger">*{{ $message }}</small>
                            </div>
                        @enderror
                    </div>
                    <div class="form-group col-lg-4 pl-0">
                        <label for="percent">Aplicar a</label>
                        <select class="form-control" id="fields" name="fields">
                            <option value="all">Ambos</option>
                            <option value="precio_lista">Precio de lista</option>
                            <option value="precio_revendedor">Precio de revendedor</option>

                        </select>
                        @error('percent')
                            <div id="percent-feedback" class="is-invalid ">
                                <small class="text-danger">*{{ $message }}</small>
                            </div>
                        @enderror
                    </div>
                    <div class="form-group col-lg-4 pl-0">
                        <label for="category">Categoria</label>
                        <select class="form-control" id="category" name="category">
                            <option value="all">Todos</option>
                            @foreach ($categorias as $categoria)
                                <option value="{{ $categoria->id }}"> {{ $categoria->nombre }}</option>
                            @endforeach
                        </select>
                        @error('category')
                            <div id="category-feedback" class="is-invalid ">
                                <small class="text-danger">*{{ $message }}</small>
                            </div>
                        @enderror
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="updateBulk">Actualizar</button>
                </div>
            </div>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous">
    </script>
    {{-- <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous">
    </script> --}}
    <script type="text/javascript">
        $(document).ready(() => {
            const urlBulk = '{{ route('api.producto.update.bulk') }}';

            const modal = $('#modal');
            const button = document.querySelector('#modalOpen');
            const buttonByBulk = document.querySelector('#updateBulk');
            button.addEventListener('click', showModal);
            buttonByBulk.addEventListener('click', sendPostBulk);

            function showModal(evt) {
                modal.modal('show');
            }

            function sendPostBulk() {
                const formData = new FormData();
                const percent = document.querySelector('#percent');
                const fields = document.querySelector('#fields');
                const category = document.querySelector('#category');
                if (!percent.value) {

                    alert('debe agregar un porcentaje');
                    return null;
                }
                if (percent.value > 100) {

                    if (!confirm('Esta agregando un valor mayor a 100, desea continuar?')) {
                        return null;

                    }
                }
                formData.append('percent', percent.value);
                formData.append('fields', fields.value);
                formData.append('category', category.value);
                fetch(urlBulk, {
                        method: 'POST',
                        body: formData,
                    })
                    .then(response => response.json())
                    .then(json => {
                        if (json.status == 'ok') {
                            location.reload();

                        } else {
                            alert('Algo salio mal. intente nuevamente.')
                        }
                    }).catch(error => {
                        alert('Error. no se actualizo el campo.');
                        console.log(error);
                    })

            }

        });
    </script>
@endsection
<script type="text/javascript">
    const url = '{{ route('api.producto.update') }}';
</script>
<script src="{{ asset('js/delete.js') }}" defer></script>
<script src="{{ asset('js/table-edit.js') }}" type="text/javascript" defer>
</script>
