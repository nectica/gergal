@extends('form.index')
@section('title', 'Actualizar estado.')
@section('title-form', 'Actualizar estado.')
@section('route-form', route('estados.update',$estado->id))
@section('content-form')
@method('PUT')
	<div class="form-group col-lg-4 pl-0">
		<label for="title">Nombre</label>
		<input type="text" class="form-control" id="descripcion" name="descripcion" aria-describedby='title-feedback'
			value="{{ old('descripcion', $estado->descripcion) }}">
		@error('descripcion')
			<div id="title-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>
	<button type="submit" class="btn btn-success mb-1">Actualizar estado</button>
@endsection
