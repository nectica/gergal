<li class="nav-item dropdown">
    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown"
        aria-haspopup="true" aria-expanded="false" v-pre>
        Abm Menu
    </a>

    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">

        @can('product-list')
            <a class="dropdown-item" href="{{ route('productos.index') }}">Ver Productos</a>
        @endcan
        @can('recipe-list')
            <a class="dropdown-item" href="{{ route('recetas.index') }}">Ver Recetas</a>
        @endcan
        @can('user-list')
            <a class="dropdown-item" href="{{ route('usuarios.index') }}">Ver Usuarios</a>
        @endcan
        @can('category-list')
            <a class="dropdown-item" href="{{ route('categorias.index') }}">Ver Categorias</a>
        @endcan
        @can('order-list')
            <a class="dropdown-item" href="{{ route('pedidos.index') }}">Ver Pedidos</a>
        @endcan
        @can('order-list')
            <a class="dropdown-item" href="{{ route('estados.index') }}">Ver Estados para pedidos</a>
        @endcan
		@can('minimo-list')
            <a class="dropdown-item" href="{{ route('minimo.index') }}">Ver mínimo de compra</a>
        @endcan
        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
        </form>
    </div>
</li>
