<li class="nav-item dropdown">
    <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
        {{ Auth::user()->name }}
    </a>

    <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
        @can(['product-create'])
            <a class="dropdown-item" href="{{ route('users.index') }}">Gestor Administrativo</a>
        @endcan
        @can('role-create')
            <a class="dropdown-item" href="{{ route('roles.index') }}">Gestor de roles</a>
        @endcan


    </div>
</li>
