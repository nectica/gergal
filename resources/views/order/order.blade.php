@if ($pedido->productos->isEmpty())
    <p class="lead">Sin productos</p>
@else
<div class="row justify-content-between">
    <div class="col-4 ">
        <h4>Producto(s):</h4>
    </div>
    <div class="col-3 offset-3 d-flex justify-content-end">
        <a href="{{ route('pedidos.excelItem', $pedido->id) }}" type="button" class="btn btn-excel-gergal">Descargar listado<i class="fas fa-file-excel"></i></a>
    </div>
</div>
<div class="row justify-content-center">
    <div class="col">
        <table class="table table-bordered">
            <thead>
                <th scope="col">Nombre</th>
                <th scope="col">Categoría </th>
                <th scope="col">Descripción </th>
                <th scope="col">Precio</th>
                <th scope="col">Cantidad </th>
            </thead>
            <tbody>
                @foreach ($pedido->productos as $producto)
                    <tr>
                        <td>{{$producto->nombre}}</td>
                        <td>{{$producto->categoriaR->nombre}}</td>
                        <td>{{$producto->descripcion}}</td>
                        <td><p class="currency">{{$producto->producto_precio_unitario}}</p></td>
                        <td>{{$producto->pivot->cant}}</td>
                    </tr>
                @endforeach
                <tr>
                    <td colspan="3" >Total</td>

                    <td colspan="2"><p class="currency">{{ $pedido->precio_total  }}</p></td>
                </tr>
            </tbody>
        </table>
    </div>
</div>
@endif
