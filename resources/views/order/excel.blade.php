<meta http-equiv="Content-type" content="text/html;charset=UTF-8" />

<table class="table table-bordered">
    <thead>
        <tr>
            <th>Pedido</th>
            <th>Usuario</th>
			<th>Dni</th>
            <th>Email</th>
			<th>Telefono</th>
			<th>Codigo de producto</th>
			<th>Producto</th>
			<th>Categoria</th>
			<th>Precio</th>
			<th>Cantidad</th>
			<th>Precio total</th>
			<th>Forma de pago</th>
			<th>Tipo de usuario</th>
			<th>Direccion</th>
			<th>Direccion2</th>
			<th>fecha creado</th>
			<th>Actualizado</th>
			<th>map</th>
			<th>Calle</th>
			<th>Localidad</th>
			<th>Partido</th>
			<th>Provincia</th>
			<th>Barrio</th>
			<th>Pais</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($pedidos as $pedido)

			@foreach ($pedido->productos as $producto)
			<tr>
				<td>{{ $pedido->id }}</td>
				<td>{{ $pedido->usuario->nombre . ' ' . $pedido->usuario->apellido }}</td>
				<td>{{ $pedido->usuario->dni }}</td>
				<td>{{ $pedido->usuario->email }}</td>
				<td>{{ $pedido->usuario->telefono }}</td>
				<td>{{ $producto->id }}</td>
				<td>{{ $producto->nombre }}</td>
				<td>{{ $producto->categoriaR->nombre }}</td>
				<td><p class="currency">{{$producto->pivot->producto_precio_unitario}}</p></td>
				<td>{{$producto->pivot->cant}}</td>
				<td>{{$pedido->precio_total}}</td>
				<td>{{$pedido->forma_pago}}</td>
				<td>{{$pedido->usuario->tipoUsuario->descripcion  }}</td>
				<td>{{$pedido->usuario->direccion }} </td>
				<td>{{$pedido->usuario->direccion2 }} </td>
				<td>{{ $pedido->fecha_creado }}</td>
				<td>{{$pedido->fecha_modif}}</td>
				<td>{{ 'https://maps.google.com/maps?q='.$pedido->usuario->latitud.','.$pedido->usuario->longitud }}</td>
				<td>{{$pedido->usuario->nombre_calle.' '.$pedido->usuario->calle_nro}}</td>
				<td>{{ $pedido->usuario->localidad }}</td>
				<td>{{ $pedido->usuario->partido }}</td>
				<td>{{ $pedido->usuario->provincia }}</td>
				<td>{{ $pedido->usuario->barrio }}</td>
				<td>{{ $pedido->usuario->pais}}</td>
			</tr>
			@endforeach

        @endforeach
    </tbody>
</table>
