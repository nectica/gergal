@extends('layouts.layout')
@section('title', $pedido->id)
@section('content')
	<div class="row  m-3">
		<div class="col-4 offset-1">
			<h2>Pedido: {{ $pedido->id }}</h2>
		</div>

	</div>
	<div class="row justify-content-start mb-2  ">
		<div class="col-lg-4 offset-2 ">
            <div class="card">
                <div class="card-header">
                    <a href="{{route('usuarios.show',$pedido->usuario->id )}}">Usuario: {{ $pedido->usuario->nombre.' '.$pedido->usuario->apellido }}</a>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Correo electrónico: {{ $pedido->usuario->email }}</li>
                    <li class="list-group-item">Teléfono: {{ $pedido->usuario->telefono }} </li>
                </ul>
            </div>

        </div>

	</div>
    @if ($pedido->rev_id)
    <div class="row justify-content-start mb-2">
        <div class="col-lg-2 offset-2">
            <div class="card">
                <div class="card-header">
                    <a href="{{route('usuarios.show',$pedido->revendedor->id )}}" >Usuario intermedario: {{ $pedido->revendedor->nombre.' '.$pedido->revendedor->apellido }}</a>
                </div>
                <ul class="list-group list-group-flush">
                    <li class="list-group-item">Correo electrónico: {{ $pedido->revendedor->email }}</li>
                    <li class="list-group-item">Teléfono: {{ $pedido->revendedor->telefono }}</li>
                </ul>
            </div>
        </div>
    </div>
    @endif

    <div class="row justify-content-center">
        <div class="col-8">
            @include('order.order')
        </div>
    </div>
@endsection
