<table class="table table-bordered">
    <thead>
        <tr>
            <th scope="col">Nombre</th>
            <th scope="col">Categoría </th>
            <th scope="col">Descripción </th>
            <th scope="col">Precio</th>
            <th scope="col">Cantidad </th>
            <th scope="col">Precio Total </th>

        </tr>
    </thead>
    <tbody>
        @foreach ($pedido->productos as $producto)
        <tr>
            <td>{{$producto->nombre}}</td>
            <td>{{$producto->categoriaR->nombre}}</td>
            <td>{{$producto->descripcion}}</td>
            <td><p class="currency">{{$producto->pivot->producto_precio_unitario}}</p></td>
            <td>{{$producto->pivot->cant}}</td>
        </tr>
        @endforeach
        <tr>
            <td colspan="3" >Total</td>

            <td colspan="2"><p class="currency">{{ $pedido->precio_total  }}</p></td>
        </tr>
        <tr>
            <td colspan="2" >Pedido creado: {{ $pedido->fecha_creado }} </td>
            <td colspan="3"></td>
        </tr>
		<tr>
            <td colspan="2" >Pedido Actualizado: {{ $pedido->fecha_creado }} </td>
            <td colspan="3"></td>
        </tr>
        <tr>
            <td colspan="2" >Usuario: {{ $pedido->usuario->nombre.' '.$pedido->usuario->apellido }} </td>
            <td colspan="2" >Dni: {{ $pedido->usuario->dni }} </td>
            <td colspan="3">Email: {{ $pedido->usuario->email }}</td>
        </tr>
		<tr>
			<td colspan="2">Tipo de usuario: {{ $pedido->usuario->tipoUsuario->descripcion }}</td>
			<td colspan="3">Direccion: {{ $pedido->usuario->direccion }}</td>
			<td colspan="3">Direccion de google Map: {{ $pedido->usuario->full_address }}</td>
		</tr>
        @if ($pedido->rev_id)
        <tr>
            <td colspan="2" >usuario intermedario: {{ $pedido->revendedor->nombre.' '.$pedido->revendedor->apellido }} </td>
            <td colspan="3">Email: {{ $pedido->revendedor->email }}</td>
        </tr>
        @endif
    </tbody>
</table>
