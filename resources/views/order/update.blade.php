@extends('form.index')
@section('title', 'Actualizar pedido.')
@section('title-form', 'Actualizar pedido.')
@section('route-form', route('pedidos.update',$pedido->id))
@section('content-form')
@method('PUT')
<div class="row  ">
    <div class="col-4 ">
        <h2>Pedido: {{ $pedido->fecha_creado }}</h2>
    </div>

</div>
<div class="row justify-content-start mb-2  ">
    <div class="col-lg-2   ">
        <div class="card">
            <div class="card-header">
                <a href="{{route('usuarios.show',$pedido->usuario->id )}}">Cliente: {{ $pedido->usuario->nombre.' '.$pedido->usuario->apellido }}</a>
            </div>
            <ul class="list-group list-group-flush">
                <li class="list-group-item">Correo electrónico: {{ $pedido->usuario->email }}</li>
                <li class="list-group-item">Teléfono: {{ $pedido->usuario->telefono }} </li>
            </ul>
        </div>

    </div>

</div>

	<div class="form-group col-lg-4 pl-0">
        <input type="hidden" name="" id="{{$pedido}}">
		<label for="estado">Estado</label>
		<select class="form-control" name="estado" id="estado" required>
			<option disabled selected>--- Seleccione una Estado ---</option>
			@foreach ($estados as $estado)
				<option value="{{ $estado->id }}" {{ old('estado', $pedido->estado) == $estado->id ? 'selected' : '' }}>{{ $estado->descripcion }}</option>
			@endforeach
		</select>
		@error('sub_categoria')
			<div id="title-feedback" class="is-invalid ">
				<small class="text-danger">*{{ $message }}</small>
			</div>
		@enderror
	</div>

	<button type="submit" class="btn btn-success mb-1">Actualizar Pedido</button>
@endsection
