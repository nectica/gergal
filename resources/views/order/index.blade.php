@extends('layouts.layout')
@section('title', 'Pagina de Pedidos')
@section('content')
    <div class="row justify-content-center">
        @if ($message = Session::get('success'))
            <div class=" col-6  mt-2 alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif

    </div>
    <div class="row justify-content-between m-3">
        <div class="col-lg-3 offset-1">
            <h2 class="font-color-gergal">Pedidos</h2>
        </div>
        <div class="col-lg-3 offset-2">
            <form action="{{ route('pedidos.filter') }}" method="POST">
                @csrf
                <div class="input-group ">
                    <input type="text" class="form-control" name="nombre" placeholder="Filtrar por cliente, email o estado">
                    <div class="input-group-append">
                        <button class="btn btn-gray " type="submit">Filtrar</button>
                    </div>
                </div>
            </form>
        </div>

        @can('order-create')
            <div class="col-lg-3  ">
                <div class="form-group col-8">
                    <label for="dias">Dias hacia atras para el reporte</label>
                    <input class="form-control" id="dias" placeholder="cantidad de dias para el reporte" value="30" />
                </div>
                <div class="form-group col-8">
                    <label for="estado">Filtrar estados para reporte</label>
                    <select class="form-control" name="estado" id="estado">
                        <option value="0" selected>Todos</option>
                        @foreach ($estados as $estado)
                            <option value={{ $estado->id }}>{{ $estado->descripcion }}</option>
                        @endforeach
                    </select>
                </div>
				<div class="form-group col-8">
                    <label for="tipo_usuario">Filtrar por tipo de usuario</label>
                    <select class="form-control" name="tipo_usuario" id="tipo_usuario">
                        <option value="0" selected>Todos</option>
                        @foreach ($tiposUsuario as $tiposUsuario)
                            <option value={{ $tiposUsuario->id }}>{{ $tiposUsuario->descripcion}}</option>
                        @endforeach
                    </select>
                </div>
                <div class="col-8">
                    <a id="download" href="{{ route('pedidos.excel') }}" type="button" class="btn btn-excel-gergal">Descargar
                        listado<i class="fas fa-file-excel"></i></a>
                </div>

            </div>
        @endcan
    </div>
    <div class="row justify-content-center">
        <div class="col-12 col-xl-10 table-responsive">
            @if ($pedidos->isEmpty())
                <p class="lead">No hay pedidos registrados.</p>
            @else
                <table class="table table-bordered">
                    <thead>
                        @if (!method_exists($pedidos, 'render'))
                            <th>Pedido</th>
                            <th>Usuario</th>
                            <th>Creado</th>
                            <th>Actualizado</th>
                            <th>Total</th>
                            <th>Estado</th>
                        @else
                            <th scope="col">@sortablelink('id', 'Pedido' )</th>
                            <th scope="col">@sortablelink('cli_id', 'Usuario')</th>
                            <th scope="col">@sortablelink('fecha_creado','Creado') </th>
                            <th scope="col">@sortablelink('fecha_modif','Actualizado') </th>
                            <th scope="col">@sortablelink('precio_total', 'Total') </th>
                            <th scope="col">@sortablelink('estado', 'Estado') </th>
                        @endif
                        <th scope="col"></th>
                    </thead>
                    <tbody>
                        @foreach ($pedidos as $pedido)
                            <tr>
                                <form id="{{ $pedido->id }}"  dblclick="handlerClick" action="">
                                    <td><a href="{{ route('pedidos.show', $pedido->id) }}">{{ $pedido->id }}</a></td>
                                    <td>{{ $pedido->usuario->nombre . ' ' . $pedido->usuario->apellido }}</td>
                                    <td>{{ $pedido->fecha_creado }}</td>
                                    <td>{{ $pedido->fecha_modif }}</td>
                                    <td>
										<p class="form-dblclick"  name="{{ 'precio_total_' . $pedido->id }}" class="currency" >
										{{ $pedido->precio_total }}</p>

                                    </td>
                                    <td>{{ $pedido->estadoR->descripcion }}</td>

                                </form>
                                <td>
                                    @can('product-edit')
                                        <a href="{{ route('pedidos.edit', $pedido->id) }}" type="button"
                                            class="btn btn-outline-primary btn-sm  my-1"> <i class="fas fa-edit"></i></a>
                                    @endcan
                                    @can('product-delete')
                                        <form name="form-delete" action="{{ route('pedidos.destroy', $pedido->id) }}"
                                            method="POST">
                                            @csrf
                                            @method('DELETE')
                                            <button style="pointer-events: none;" type="submit"
                                                class="btn btn-outline-danger btn-sm" disabled><i
                                                    class="fas fa-trash-alt"></i></button>
                                        </form>
                                    @endcan

                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            @endif
            {{ method_exists($pedidos, 'appends') ? $pedidos->appends(\Request::except('page'))->render() : '' }}

        </div>

    </div>

    <script type="text/javascript">
        const downloadExcel = document.querySelector('#download');
        downloadExcel.addEventListener('click', function() {
            const days = document.querySelector('#dias').value;
            const state = document.querySelector('#estado').value;
            const userType = document.querySelector('#tipo_usuario').value;
            let url = `<?= route('pedidos.excel') ?>/${days}/${state}/${userType}`;
            console.log(url);
            downloadExcel.href = url;

            return false;
        });
    </script>
@endsection
<script src="{{ asset('js/delete.js') }}" defer>
</script>

<script src="https://unpkg.com/ionicons@5.0.0/dist/ionicons.js"></script>
