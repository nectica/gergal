@extends('layouts.layout')
@section('title', 'Pagina principal')
@section('content')
    <div class="row justify-content-between m-3">
        <div class="col-lg-3 offset-1">
            <h2 class="font-color-gergal">Pagina Principal</h2>
        </div>

    </div>
    <div class="row justify-content-center">
        <div class="col-12 col-xl-10 table-responsive ">
            <div class="text-center" id="spinner-chart">
                <div class="spinner-grow text-success ml-auto" style="width: 3rem; height: 3rem;" role="status">
                </div>
            </div>
            <div class="demo-container">
                <div id="chart"></div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-8">
            <div class="d-flex align-items-center" id="spinner-pie">
                <strong>Cargando Gráfico...</strong>
                <div class="spinner-grow text-primary ml-auto" style="width: 3rem; height: 3rem;" role="status">
                </div>
            </div>
            <div class="demo-container">
                <div id="pie"></div>
            </div>
        </div>
    </div>
    <div class="row  m-3">
        <div class=" card col-lg-2 offset-1 mb-2">
            <div class="card-body">
                <h5 class="card-title font-color-gergal">Numero de Pedidos registrados</h5>
                <h6 class="card-subtitle mb-2 text-muted">en los últimos 7 días</h6>
                <h1 class="card-text text-center mt-4"><a class="font-color-gergal" href=""> {{ $pedidos }}</a> </h1>
            </div>
        </div>
        <div class=" card col-lg-2 offset-1 mb-2">
            <div class="card-body">
                <h5 class="card-title font-color-gergal">Consultas creadas</h5>
                <h6 class="card-subtitle mb-2 text-muted">en los últimos 7 días</h6>
                <h1 class="card-text text-center mt-4"><a class="font-color-gergal" href="">{{ $consultas }}</a></h1>
            </div>
        </div>
        <div class=" card col-lg-2 offset-1 mb-2  ">
            <div class="card-body">
                <h5 class="card-title font-color-gergal">Productos registrados</h5>
                <h6 class="card-subtitle mb-2 text-muted"></h6>
                <h1 class="card-text text-center mt-4"><a class="font-color-gergal" href="">{{ $productos }}</a> </h1>
            </div>
        </div>
        <div class=" card col-lg-2 offset-1 mb-2">
            <div class="card-body">
                <h5 class="card-title font-color-gergal">Recetas registrados </h5>
                <h6 class="card-subtitle mb-2 text-muted"></h6>
                <h1 class="card-text text-center mt-4"><a class="font-color-gergal" href="">{{ $recetas }}</a></h1>
            </div>
        </div>
    </div>
    <script type="text/javascript" src="devExpress/lib/js/jquery-2.2.3.js"></script>
    <script type="text/javascript" src="devExpress/lib/js/dx.viz-web.js"></script>
    <script type="text/javascript" src="devExpress/lib/js/globalize.min.js"></script>
    <script type="text/javascript" src="devExpress/lib/js/jszip.js"></script>
    <script language="JavaScript" type="text/javascript">
        const $j = $.noConflict();

        (function() {
            const spinnerPie = document.getElementById('spinner-pie');
            const spinnerChart = document.getElementById('spinner-chart');

            const today = new Date();
            var dataSourcePedidos = <?= json_encode($pedidosByDays) ?>;
			const days = new Date(today.getFullYear(), today.getMonth() + 1, 0).getDate();
            const dataSourcePedidosByDay = new Array();
            for (let index = 0; index < days; index++) {
                dataSourcePedidosByDay.push({
                    total: dataSourcePedidos[index + 1] ? dataSourcePedidos[index + 1].count : 0,
                    day: "" + (index + 1),
                    amount: dataSourcePedidos[index + 1] ? dataSourcePedidos[index + 1].amount : 0,

                });


            }
            $j("#chart").dxChart({
                dataSource: dataSourcePedidosByDay,
                commonSeriesSettings: {
                    argumentField: "day",
                    type: "bar",
                    hoverMode: "allArgumentPoints",
                    selectionMode: "allArgumentPoints",
                    label: {
                        visible: true,
                        format: {
                            type: "fixedPoint",
                            precision: 0
                        },

                    }
                },
                series: [{
                        valueField: "total",
                        name: "Nùmero de ordenes"
                    },
                    {
                        valueField: "amount",
                        name: "monto total vendido"
                    },
                ],
                title: "Pedidos realizados mes actual",
                legend: {
                    verticalAlignment: "bottom",
                    horizontalAlignment: "center"
                },
                "export": {
                    enabled: true
                },

				valueAxis:[ {label: {format: "currency"}}, {label: {format: "number"}}],
                onPointClick: function(e) {
                    e.target.select();
                }
            });

            var dataSourceProductos = <?= json_encode($productosByName) ?>;
			$j("#pie").dxPieChart({

                palette: "bright",
                dataSource: dataSourceProductos,
                series: [{
                    argumentField: "name",
                    valueField: "count",
                    label: {
                        visible: true,
                        connector: {
                            visible: true,
                            width: 1
                        }
                    }
                }],
                title: "Productos con mas ordenes generadas",
                "export": {
                    enabled: true
                },
                onPointClick: function(e) {
                    var point = e.target;

                    toggleVisibility(point);
                },
                onLegendClick: function(e) {
                    var arg = e.target;

                    toggleVisibility(this.getAllSeries()[0].getPointsByArg(arg)[0]);
                }
            });
            spinnerChart.setAttribute('style', 'display: none !important');
            spinnerPie.setAttribute('style', 'display: none !important');

            function toggleVisibility(item) {
                if (item.isVisible()) {
                    item.hide();
                } else {
                    item.show();
                }
            }
        })($j);

    </script>
@endsection
