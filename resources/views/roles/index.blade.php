@extends('layouts.layout')
@section('content')

<div class="row justify-content-between my-3">
    <div class="col-md-2 offset-lg-1">
        <h2 class="font-color-gergal">Roles</h2>
    </div>

    <div class="col-md-2 " style="padding: 0">
        <a class="btn btn-success-gergal" href="{{ route('roles.create')}}"> Crear rol</a>
    </div>

</div>

@if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
@endif
<div class="row justify-content-center">
    <div class="col-12 col-xl-10 table-responsive">
        <table class="table  table-bordered">
          <thead>
             <th >No</th>
             <th >Nombre</th>
             <th  width="200">Acción</th>
          </thead>
        @foreach ($roles as $key => $role)
            <tr>
                <td>{{ ++$i }}</td>

                <td class="text-gergal-color">{{ $role->name }}</td>
                <td>
                    <a class="btn btn-outline-success" href="{{ route('roles.show',$role->id) }}"><i class="far fa-eye"></i></a>
                    @can('role-edit')
                        <a class="btn btn-outline-primary " href="{{ route('roles.edit',$role->id) }}"><i class="fas fa-edit"></i></a>
                    @endcan
                    @can('role-delete')
                        {!! Form::open(['method' => 'DELETE','route' => ['roles.destroy', $role->id],'style'=>'display:inline', 'name' =>"form-delete"]) !!}
                            <button type="submit" class="btn btn-outline-danger btn-sm" disabled><i class="fas fa-trash-alt"></i></button>
                        {!! Form::close() !!}
                    @endcan
                </td>
            </tr>
            @endforeach
        </table>
        {!! $roles->render() !!}
    </div>
</div>
@endsection
<script src="{{ asset('js/delete.js') }}" defer></script>
