@extends('layouts.layout')
@section('content')
<div class="row justify-content-center ">
    <div class="col-md-8 margin-tb">
        <div class="pull-left">
            <h2>Rol</h2>
        </div>
        <div class="pull-right">
            <a class="btn btn-primary" href="{{ route('roles.index') }}">Regresar</a>
        </div>
    </div>
</div>

<div class="row justify-content-center"  >
    <div class="col-xs-8 col-sm-8 col-md-8">
        <div class="form-group">
            <strong>Nombre:</strong>
            {{ $role->name }}
        </div>
    </div>
    <div class="col-xs-8 col-sm-8 col-md-8">
        <div class="form-group">
            <strong>Permisos:</strong>
            @if(!empty($rolePermissions))
                @foreach($rolePermissions as $v)
                    <label class="label label-success">{{ $v->name }},</label>
                @endforeach
            @endif
        </div>
    </div>
</div>
@endsection
