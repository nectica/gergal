@extends('layouts.layout')
@section('content')
    <div class="row justify-content-center mt-3  mx-0">
        <div class="col-lg-8">
            <h3 class="font-color-gergal">
                @yield('title-form')
            </h3>
            @include('partials.alerts.error')
        </div>
    </div>
    <div class="row justify-content-center mt-3 mx-0">
        <div class="col-8">
            <form method="POST" action=" @yield('route-form') " enctype="multipart/form-data">
                @csrf
                @yield('content-form')
            </form>
        </div>
    </div>
@endsection
