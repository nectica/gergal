<?php

use App\Http\Controllers\api\ProductoController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('producto/update', [ProductoController::class, 'update'])->name('api.producto.update');
Route::post('producto/update/bulk', [ProductoController::class, 'updateByBulk'])->name('api.producto.update.bulk');
