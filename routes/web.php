<?php

use App\Http\Controllers\CategoriaController;
use App\Http\Controllers\ContactoFormController;
use App\Http\Controllers\EstadoController;
use App\Http\Controllers\MinimoCompraController;
use App\Http\Controllers\PedidoController;
use App\Http\Controllers\ProductoController;
use App\Http\Controllers\RecetaController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UsuarioController;
use App\Models\Usuario;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);

    Route::get('productos/filter',[ ProductoController::class, 'filter'])->name('productos.filter.get');
    Route::resource('productos', ProductoController::class)->names('productos');
    Route::post('productos/filter',[ ProductoController::class, 'filter'])->name('productos.filter');
    Route::resource('categorias', CategoriaController::class )->names('categorias');
    Route::post('categorias/filter',[ CategoriaController::class, 'filter'])->name('categorias.filter');
    Route::resource('recetas', RecetaController::class )->names('recetas');
    Route::post('recetas/filter',[ RecetaController::class, 'filter'])->name('recetas.filter');
    Route::resource('usuarios', UsuarioController::class )->names('usuarios');
    Route::post('usuarios/filter',[ UsuarioController::class, 'filter'])->name('usuarios.filter');
    Route::resource('consultas', ContactoFormController::class )->parameters(['consultas' => 'contacto',])->names('consultas');
    Route::post('consultas/filter',[ ContactoFormController::class, 'filter'])->name('consultas.filter');
    Route::get('pedidos/filter',[ PedidoController::class, 'filter'])->name('pedidos.filter.get');
    Route::resource('pedidos', PedidoController::class )->names('pedidos');
    Route::get('pedidos/excel/download/productos/{id}', [ PedidoController::class, 'exportItem'])->name('pedidos.excelItem');
    Route::get('pedidos/excel/download/{days?}/{state?}/{tiposUsuario?}', [ PedidoController::class, 'export'])->where('days', '[0-9]+')->name('pedidos.excel');
    Route::post('pedidos/filter',[ PedidoController::class, 'filter'])->name('pedidos.filter');

    Route::resource('estados', EstadoController::class );
    Route::post('estados/filter',[ EstadoController::class, 'filter'])->name('estados.filter');

	Route::resource('minimo', MinimoCompraController::class)->names('minimo');
    Route::post('minimo/filter',[ MinimoCompraController::class, 'filter'])->name('minimo.filter');


    Route::get('usuario/load', [UsuarioController::class, 'loadAddress']);
});
