<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;


class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'product-list',
            'product-create',
            'product-edit',
            'product-delete',
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'recipe-list',
            'recipe-create',
            'recipe-edit',
            'recipe-delete',
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            'category-list',
            'category-create',
            'category-edit',
            'category-delete',
            'contact-list',
            'contact-create',
            'contact-edit',
            'contact-delete',
            'order-list',
            'order-create',
            'order-edit',
            'order-delete',
            'status-list',
            'status-create',
            'status-edit',
            'status-delete',
			'minimo-list',
            'minimo-create',
            'minimo-edit',
            'minimo-delete',
         ];
         foreach ($permissions as $permission) {
            Permission::create(['name' => $permission]);

         }
    }
}
